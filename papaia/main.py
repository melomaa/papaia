"""
Runner for API parser
"""
import argparse
import re
import yaml
from papaia.api_parser import ApiParser
from papaia.utils import error_print
from papaia.reporter import output_database
from papaia.collector import create_robot_base

VERSION_NUMBER = "0.2.0"

DEFAULT_CONFIG = 'papaia_config.yaml'
MALFORMED_SRC = "Malformed source file path."
MALFORMED_OUT = "Malformed output path, reverting to stdout."


def get_configuration(filename, collect):
    """
    Read configuration from file and convert to dictionary

    :param filename: Configuration file
    :param collect: If running in test data collection mode
    :return: Configuration dictionary
    """
    try:
        with open(filename, 'r') as cfg_file:
            config = yaml.safe_load(cfg_file)
    except (FileNotFoundError, OSError):
        if filename == DEFAULT_CONFIG:
            if not collect:
                print("\n!!!! No configuration file available for filters.  !!!!\n")
            config = None
        else:
            raise RuntimeError("Failed to read configuration from %s" % filename)
    return config


def init_argparse():
    """
    Initialize command line argument parsing

    :return: Argument parser instance
    """
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [FILE]...",
        description="Parse API calls and related input."
    )
    parser.add_argument(
        "-v", "--version", action="version", version=f"Papaia version {VERSION_NUMBER}"
    )
    parser.add_argument(
        "-f", "--format", nargs='?', default='markdown',
        choices=['markdown', 'html'], help="Output format (markdown, html)"
    )
    parser.add_argument(
        "-o", "--output", nargs='?', help="Output file"
    )
    parser.add_argument(
        "-c", "--config", nargs='?', help="Configuration file"
    )
    parser.add_argument(
        "-t", "--test", action='store_true',
        help="""Create collection of information for tests.
                No report on input validation is generated"""
    )
    parser.add_argument('files', nargs='*')
    return parser


def check_argument_safe(arg_str):
    """
    Check that argument string is safe

    :param arg_str: String to be checked
    :return: True if string is safe
    """
    result = False
    if isinstance(arg_str, str):
        check = re.search('[^\\w\\s\\-\\/\\.]', arg_str)
        if check is None:
            result = True
    return result


def main():
    """
    Main function

    :return: 0
    """
    parser = init_argparse()
    args = parser.parse_args()
    database = {}
    config_file = DEFAULT_CONFIG
    if args.config and check_argument_safe(args.config):
        config_file = args.config
    try:
        config = get_configuration(config_file, args.test)
        api_parser = ApiParser(config)
        for file in args.files:
            if not check_argument_safe(file):
                raise RuntimeError(MALFORMED_SRC)
            database[file] = api_parser.find_calls(file)
            if args.output and not check_argument_safe(args.output):
                error_print(MALFORMED_OUT)
                args.output = None
            if args.test:
                create_robot_base(database[file], args.output)
            else:
                output_database(database, out_format=args.format, output_file=args.output)
    except RuntimeError as err:
        error_print(str(err))
    return 0


if __name__ == '__main__':  # pragma: no cover
    main()
