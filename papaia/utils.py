"""
Utility functions for API parser
"""
import re

MAX_IF_LINES = 5
IF_INDENTATION = re.compile('^(\\s*)(?:if|elif)\\s')
IF_LINE_COMPLETE = re.compile('^\\s*(?:if|elif)\\s.*(?::\\s*|:\\s*#.*)$')
IF_ENDING = re.compile('^.*(?::\\s*|:\\s*#.*)$')
FIELD_USE = '{}(?:\\[|\\.get\\()\\s*[\'"]([^\\)\\]]+?)[\'"]'
FIELD_END = '[^\\)\\]]*?(?:\\]|\\))'
VARIABLE_STATUS = {'safe': 0, 'unknown': -1, 'threat': -2}


def field_is_safe(match, input_var):
    """
    Check if field is safe to use

    :param match: Regex match of the field
    :param input_var: Input variable information containing list of safe fields
    :return: True or False
    """
    return len(match.groups()) == 2 and any(
        field['name'] == match.group(2) for field in input_var['safe_fields'])


def variable_is_safe(status, indent):
    """
    Check if variable is safe to use with given indentation level

    :param status: Variable status
    :param indent: Indentation of the current line
    :return: True or False
    """
    return VARIABLE_STATUS['unknown'] < status < indent


def update_variable_status(input_var, new_status):
    """
    Update the status of given input variable, if new status has higher priority

    :param input_var: Input variable information
    :param new_status: Proposed status
    """
    if (new_status == VARIABLE_STATUS['unknown'] and
            input_var['status'] > VARIABLE_STATUS['safe']) \
            or input_var['status'] == VARIABLE_STATUS['unknown']:
        input_var['status'] = new_status


def remove_safe(param_list, input_var, indent):
    """
    Remove safe variables from the list of parameters

    :param param_list: List of function parameters
    :param input_var: Input variable information
    :param indent: Line indentation
    :return: Filtered list of function parameters
    """
    if input_var['name'] in param_list and variable_is_safe(
            input_var['status'], indent):
        param_list[:] = (entry for entry in param_list if input_var['name'] != entry)
        input_var['stats']['filtered'] += 1
    return param_list


def filter_function_params(params, input_var, indent):
    """
    Filter function parameters for nested function calls

    :param params: List of function parameters
    :param input_var: Input variable information
    :param indent: Line indentation
    :return: Filtered list of parameters, list of removed parameters
    """
    nested = []
    for param in params:
        match = re.search('\\S+\\([^)]*?{}'.format(input_var['name']), param)
        if match:
            nested.append(param)
    params[:] = (entry for entry in params if entry not in nested)
    params = remove_safe(params, input_var, indent)
    return params, nested


def handle_known_filter(line, input_var, params):
    """
    Check input use with known filter function

    :param line: Source line to check
    :param input_var: Input variable information
    :param params: Parameters passed to filter function
    """
    if input_var['name'] in params:
        update_variable_status(input_var, get_indent(line))
    else:
        fuse_regex = re.compile(FIELD_USE.format(input_var['name']))
        for item in params:
            field = fuse_regex.search(item)
            if field and not any(safe_field['name'] == field.group(1)
                                 for safe_field in input_var['safe_fields']):
                add_safe_field(input_var, field.group(1), get_indent(line))


def get_func_and_params(line, func_regex, var_name):
    """
    Get function name and list of parameters related to input variable

    :param line: Line of code to check
    :param func_regex: Regular expression used for extracting function name
    :param var_name: Name of the input variable to check for
    :return: Function name, list of parameters and offset to next character to process.
    """
    func = ''
    params = []
    pos = len(line)
    match = func_regex.search(line)
    if match:
        func = match.group(1)
        open_cnt = 1
        close_cnt = 0
        pos = start = line.find(func + '(') + len(func)
        while open_cnt != close_cnt and pos < len(line) - 1:
            pos += 1
            if line[pos] == '(':
                open_cnt += 1
            if line[pos] == ')':
                close_cnt += 1
        param_str = line[start + 1:pos]
        params = get_valid_params(param_str, var_name)
    return func, params, pos


def check_parameter_use(line, func, input_var, params, filter_config):
    """
    Check if unsafe parameters are passed to a function

    :param line: Line to check
    :param func: Function name
    :param input_var: Input variable information
    :param params: List of function parameters
    :param filter_config: Filter configuration dictionary
    :return:
    """
    line_safe = False
    if not params:
        line_safe = True
    elif 'KNOWN_FILTERS' in filter_config and func in filter_config['KNOWN_FILTERS']:
        handle_known_filter(line, input_var, params)
        line_safe = True
    elif 'KNOWN_SAFE' in filter_config and func in filter_config['KNOWN_SAFE']:
        input_var['stats']['safe'] += 1
        line_safe = True
    elif input_var['safe_fields'] and input_var['name'] not in params:
        line_safe = check_safe_fields(input_var, params)
        input_var['stats']['filtered'] += 1
    return line_safe


def check_for_method(line, input_var, valid_methods):
    """
    Check for use of known filter methods

    :param line: Source line to check
    :param input_var: Dictionary of input variable information
    :param valid_methods: List of known validation methods
    :return: Whether match was found
    """
    match = False
    for method in valid_methods:
        method_check = input_var['name'] + '.' + method
        if method_check in line:
            update_variable_status(input_var, get_indent(line))
            match = True
        field_regex = re.compile(FIELD_USE.format(input_var['name']) + FIELD_END + '\\.' + method)
        field_matches = field_regex.findall(line)
        indent = get_indent(line)
        for field_match in field_matches:
            add_safe_field(input_var, field_match, indent)
            match = True
    return match


def add_safe_field(input_var, field_name, indent):
    """
    Add a field to the list of safe fields of a input variable

    :param input_var: Input variable information
    :param field_name: Name of the safe field
    :param indent: Line indentation
    """
    input_var['safe_fields'].append({'name': field_name, 'indent': indent})


def get_indent(line):
    """
    Get the indentation of the line
    :param line: Line to process
    :return: Number of whitespace characters for indentation
    """
    return len(line) - len(line.lstrip())


def check_safe_fields(input_var, params):
    """
    Check for the use of safe fields

    :param input_var: Dictionary of input variable information
    :param params: List of relevant function parameters
    :return: True for safe usage, otherwise False
    """
    is_safe = False
    used_fields = []
    fuse_regex = re.compile(FIELD_USE.format(input_var['name']))
    for param in params:
        used_fields.extend(fuse_regex.findall(param))
    sf_cnt = 0
    for item in used_fields:
        if not any(safe_field['name'] == item for safe_field in input_var['safe_fields']):
            break
        sf_cnt += 1
    if sf_cnt == len(params):
        is_safe = True
    return is_safe


def param_split(param_str):
    """
    Split function parameters to a list without breaking nested functions

    :param param_str: String of function parameters
    :return: List of parameters
    """
    param_list = []
    if '(' in param_str:
        begin = 0
        pos = 0
        open_cnt = 0
        close_cnt = 0
        while pos < len(param_str):
            if (param_str[pos] == ',' or param_str[pos] == '%') and open_cnt == close_cnt:
                param_list.append(param_str[begin:pos].strip())
                begin = pos + 1
            elif param_str[pos] == '(':
                open_cnt += 1
            elif param_str[pos] == ')':
                close_cnt += 1
            pos += 1
        param_list.append(param_str[begin:pos].strip())
    else:
        param_list = [item.strip() for item in re.split(',| % ', param_str)]
    return param_list


def get_valid_params(param_str, var_name):
    """
    Extract valid parameters from function parameter string

    :param param_str: All function parameters as a string
    :param var_name: Name of the variable to check
    :return: List of valid uses of given variable
    """
    param_list = param_split(param_str)
    valid_list = []
    valid_regex = re.compile('["\'][^"\']+["\']|({})'.format(var_name))
    for entry in param_list:
        match_list = valid_regex.findall(entry)
        for item in match_list:
            if item == var_name:
                valid_list.append(entry)
    return valid_list


def concat_multiline_if(lines, index):
    """
    Check for multiline if statement and combine lines.

    :param lines: Collection of lines
    :param index: Index to line after if in the collection
    :return: if statement as single line
    """
    line = lines[index - 1]
    match = IF_LINE_COMPLETE.match(line)
    if not match:
        try:
            for offset in range(0, MAX_IF_LINES):
                ending = IF_ENDING.match(lines[index + offset])
                if ending:
                    line = line.rstrip()
                    for part in range(index, index + offset + 1):
                        line += ' ' + lines[part].strip()
                    index += offset + 1
                    break
        except IndexError:
            pass
    return line, index


def line_completion(lines, index):
    """
    Combine multiline code to one line based on matching parentheses

    :param lines: List of code lines
    :param index: Index to next line
    :return: A complete line of code and index to next line
    """
    line = lines[index - 1]
    open_par = line.count('(')
    close_par = line.count(')')
    try:
        offset = 0
        while open_par != close_par and offset < 5:
            open_par += lines[index + offset].count('(')
            close_par += lines[index + offset].count(')')
            offset += 1
        if offset < 5:
            line = line.rstrip()
            for part in range(0, offset):
                line += ' ' + lines[index + part].strip()
            index += offset
    except IndexError:
        pass
    match = IF_INDENTATION.match(line)
    if match:
        line, index = concat_multiline_if(lines, index)
    return line, index


def error_print(msg):
    """
    Print error message

    :param msg: Message to print
    """
    print('\nPapaia error:')
    print(msg)
    print('\n')
