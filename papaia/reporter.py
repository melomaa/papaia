"""
API parser report generation
"""
from os import path as os_path
from jinja2 import Environment, FileSystemLoader, PackageLoader, ChoiceLoader, select_autoescape
import markdown   # pylint: disable=E0401


def render_stats(stats):
    """
    Render stats for text output

    :param stats: Statistics dictionary
    :return: Text (markdown) output of stats
    """
    open_inputs = stats['inputs'] - stats['filtered'] - stats['misses'] - stats['safe']
    output = '__Routes__ Total: %d Parsed: %d Unparsed: %d Warnings: %d\n\n' % (
        stats['routes'], stats['parsed'], (stats['routes'] - stats['parsed']), stats['concerns'])
    output += '__Inputs__ Total: %d  Filtered: %d, Safe %d, Not checked: %d, Open: %d\n\n' \
              % (stats['inputs'], stats['filtered'], stats['safe'],
                 stats['unknown'], open_inputs)
    output += '\n'
    return output


def render_concerns(file_data):
    """
    Render concerns in markdown format

    :param file_data: Source file information dictionary
    :return: Concerns in markdown string, concern count
    """
    output = ''
    count = 0
    for route in file_data['routes']:
        if file_data['routes'][route]['concerns']:
            count += 1
            output += route + ' (line ' + str(file_data['routes'][route]['line']) + ')'
            output += '\n-------------------\n\n'
            for item in file_data['routes'][route]['concerns']:
                output += ' * ' + item + '\n'
            output += '\n'
    return output, count


def generate_html_report(file, concern_str, stats, date_str):
    """
    Generate report as HTML page

    :param file: Path of the scanned source file
    :param concern_str: Markdown string of concerns
    :param stats: Scan statistics
    :param date_str: Scan date as a string
    :return: Report as a HTML page
    """
    loaders = []
    try:
        pkg_loader = PackageLoader('papaia', './')
        loaders.append(pkg_loader)
    except (KeyError, ModuleNotFoundError):
        pass
    loaders.append(FileSystemLoader('./'))
    jinja_env = Environment(
        loader=ChoiceLoader(loaders),
        autoescape=select_autoescape(['html', 'xml'])
    )
    md_processor = markdown.Markdown()
    concern_md = md_processor.convert(concern_str)
    stats['unparsed'] = (stats['routes'] - stats['parsed'])
    stats['open'] = stats['inputs'] - stats['filtered'] - stats['misses'] - stats['safe']
    template = jinja_env.get_template('api_template.html')
    return template.render(file=os_path.basename(file), concerns=concern_md,
                           stats=stats, date=date_str)


def write_output(output, output_file=None):
    """
    Write output to file or stdout

    :param output: Output string
    :param output_file: Name of the file to write [Optional]
    """
    if output_file and output:
        with open(output_file, 'w') as out_file:
            out_file.write(output)
    else:
        print(output)


def output_database(database, out_format='markdown', output_file=None):
    """
    Generate output of the database

    :param database: Database dictionary
    :param out_format: Format of the output
    :param output_file: Output file
    :return: N/A
    """
    output = ''
    for file in database:
        concern_str, database[file]['stats']['concerns'] = render_concerns(database[file])
        if out_format == 'html':
            output = generate_html_report(
                file, concern_str, database[file]['stats'],
                database[file]['date'].strftime("on  %m/%d/%Y at %H:%M:%S"))
            break
        output += 'API report for ' + file + '\n====================\n\n'
        output += database[file]['date'].strftime("Generated on  %m/%d/%Y at %H:%M:%S\n\n")
        output += render_stats(database[file]['stats'])
        output += concern_str + '\n\n'
    write_output(output, output_file)
