"""
API parser with security scanner for request inputs
"""
from datetime import datetime
import re
import papaia.utils as utils
from papaia.utils import VARIABLE_STATUS

SKIP_LINE_TAG = 'api_parser: skip'

THREAT_STR = '%s : (%s) passed to **%s**'
PATH_REGEX = re.compile('\\s*["\'](.+?)["\']')
SERVER_REGEX = re.compile('^\\s*(\\w+?)\\s*=\\s*(?:flask\\.)?Flask\\(')
SKIP_LINE_REGEX = re.compile('\\s#\\s{1,4}' + SKIP_LINE_TAG)

INPUT_REGEX = re.compile(
    '["\'][^"\']+["\']|(request\\.(?:form|json|args|query_string|values|get_(?:json|data)))')
FUNC_USE_FOR = '\\s*([^"\'\\s\\(]+)\\(.*{}.*\\).*'
ASSIGN_VAR = '\\s*(.+)\\s*=\\s*{}($|\\(|[\\.\\[\\s]+).*'


class ApiParser:
    """
    Class for parsing REST API
    """

    def __init__(self, config=None):
        """
        Constructor

        :param config: Configuration dictionary
        """
        self.routes = {}
        self.stats = {}
        self.candidates = []
        self.server_name = ''
        self.path = ''
        self.filter_config = {
            'KNOWN_FILTERS': [],
            'KNOWN_CHECK_METHODS': [],
            'KNOWN_SAFE': []
        }
        if config:
            for key in config.keys():
                if key in self.filter_config:
                    self.filter_config[key] = config[key]

    def record_concern(self, input_var, func_name):
        """
        Record found unsafe use of a variable

        :param input_var: Input variable information in a dictionary
        :param func_name: Name of the function with unsafe input
        """
        finding = THREAT_STR % (input_var['desc'], input_var['name'], func_name)
        self.routes[self.path]['concerns'].append(finding)
        input_var['stats']['threats'] += 1

    def handle_nested(self, nested, input_var, indent, func_regex):
        """
        Handle nested function calls with input variable in parameters

        :param nested: List of nested function calls
        :param input_var: Input variable information
        :param indent: Line indentation
        :param func_regex: Regular expression to extract function name
        """
        for nested_line in nested:
            nested_func, nested_params, _ = utils.get_func_and_params(nested_line, func_regex,
                                                                      input_var['name'])
            nested_params, next_nested = utils.filter_function_params(nested_params,
                                                                      input_var, indent)
            nested_safe = utils.check_parameter_use(nested_line, nested_func, input_var,
                                                    nested_params, self.filter_config)
            if not nested_safe:
                self.record_concern(input_var, nested_func)
            if next_nested:
                self.handle_nested(next_nested, input_var, indent, func_regex)

    def check_for_function(self, line, input_var):
        """
        Check if input data is passed to a function and analyze implications

        :param line: Source line to check
        :param input_var: Dictionary of input variable information
        :return: Whether match was found
        """

        func_regex = re.compile(FUNC_USE_FOR.format(input_var['name']))
        begin = 0
        indent = utils.get_indent(line)
        while begin < len(line):
            func, params, offset = utils.get_func_and_params(line[begin:], func_regex,
                                                             input_var['name'])
            if func and params:
                params, nested = utils.filter_function_params(params, input_var, indent)
                line_safe = utils.check_parameter_use(line, func, input_var,
                                                      params, self.filter_config)
                if not line_safe:
                    self.record_concern(input_var, func)
                if nested:
                    self.handle_nested(nested, input_var, indent, func_regex)
            begin += offset + 1

    def check_for_assignment(self, line, input_var):
        """
        Check if input data is assigned to another variable

        :param line: Source line to check
        :param input_var: Dictionary of input variable information
        """
        assign_regex = re.compile(ASSIGN_VAR.format(input_var['name']))
        assign_match = assign_regex.search(line)
        if assign_match:
            assigned_to = assign_match.group(1).strip()
            self.add_candidate(assigned_to, input_var['desc'], parent=input_var['name'])
            input_var['children'].append(assigned_to)

    def analyze_candidates(self, line):
        """
        Analyze use of input variables

        :param line: Source line to check
        :return: N/A
        """
        indent = utils.get_indent(line)
        for input_var in self.candidates:
            if input_var['safe_fields']:
                input_var['safe_fields'][:] = (entry for entry in input_var['safe_fields']
                                               if indent > entry['indent'])
            if input_var['status'] > 0 and indent <= input_var['status']:
                utils.update_variable_status(input_var, VARIABLE_STATUS['unknown'])

            if input_var['name'] in line:
                self.check_for_function(line, input_var)
                utils.check_for_method(line, input_var, self.filter_config['KNOWN_CHECK_METHODS'])
                self.check_for_assignment(line, input_var)

    def candidate_index(self, name):
        """
        Get candidate index by name

        :param name: Name of the candidate
        :return: Index of the candidate or -1 if not found
        """
        index = -1
        for idx, entry in enumerate(self.candidates):
            if entry['name'] == name:
                index = idx
                break
        return index

    def add_candidate(self, name, desc, parent=None):
        """
        Add input variable to the list of candidates to check for threats

        :param name: Name of the variable
        :param desc: Description for the variable
        :param parent: Parent if assigned from another variable [Optional]
        """
        added_new = False
        index = self.candidate_index(name)
        if index == -1:
            candidate = {'name': name, 'desc': desc, 'status': VARIABLE_STATUS['unknown'],
                         'safe_fields': [], 'children': [],
                         'stats': {'filtered': 0, 'threats': 0, 'safe': 0}}
            if parent:
                candidate['parent'] = parent
            index = len(self.candidates)
            self.candidates.append(candidate)
            added_new = True
        return index, added_new

    def get_server_name(self, lines):
        """
        Find name of the server instance

        :param lines: Source code lines
        :return: True if name is found
        """
        status = False
        for line in lines:
            match = SERVER_REGEX.match(line)
            if match:
                self.server_name = match.group(1)
                status = True
                break
        return status

    def parse_api_line(self, line, line_num):
        """
        Parse information from API route definition

        :param line: Line containing the route definition
        :param line_num: Line index
        :return: API path
        """
        path = ''
        method_list = []
        self.candidates = []
        self.stats['routes'] += 1
        if not SKIP_LINE_REGEX.search(line):
            try:
                route_str = line[line.find('(') + 1:line.rfind(')')]
                path = PATH_REGEX.match(route_str).group(1)
                if 'methods' in route_str:
                    method_start = route_str.index('methods=[') + 9
                    m_str = route_str[method_start:route_str.find(']', method_start)]
                    methods = re.findall(r'["\'](.*?)["\']', m_str)
                    method_list.extend(methods)
                else:
                    method_list.append('GET')
                self.routes[path] = {'methods': method_list.copy(), 'line': line_num,
                                     'args': [], 'concerns': []}
            except (AttributeError, ValueError, IndexError):
                path = ''
        return path

    def do_analysis(self, line):
        """
        Run analysis for the line

        :param line: Line containing the route definition
        """
        if not SKIP_LINE_REGEX.search(line):
            inputs = INPUT_REGEX.findall(line)
            for item in inputs:
                if item and self.add_candidate(item, 'Input')[1]:
                    self.stats['inputs'] += 1
            if self.candidates:
                self.analyze_candidates(line)
            if line.lstrip().startswith('return '):
                self.path_post_process()

    def combine_children(self, item):
        """
        Combine statistics from children under parent variable

        :param item: Input variable to check for children
        """
        for child in item['children']:
            child_idx = self.candidate_index(child)
            if child_idx > -1:
                for key in self.candidates[child_idx]['stats'].keys():
                    item['stats'][key] += self.candidates[child_idx]['stats'][key]

    def path_post_process(self):
        """
        Process the end of API path function
        """
        for item in self.candidates:
            if 'parent' in item:
                continue
            self.combine_children(item)
            if item['stats']['threats'] == 0:
                if item['stats']['filtered'] > 0:
                    self.stats['filtered'] += 1
                elif item['stats']['safe'] > 0:
                    self.stats['safe'] += 1
                else:
                    self.stats['unknown'] += 1
        self.path = ''
        self.candidates = []

    def analyse_lines(self, lines):
        """
        Analyse lines for API calls

        :param lines: Lines from an API file
        """
        self.path = None
        index = 0
        while index < len(lines):
            line = lines[index]
            index += 1
            if line.lstrip().startswith('#'):
                continue
            line, index = utils.line_completion(lines, index)
            if line.lstrip().startswith('@{}.route'.format(self.server_name)):
                self.path = self.parse_api_line(line, index)
                continue
            if self.path:
                self.do_analysis(line)

    def find_calls(self, file):
        """
        Scan for API calls and analyze the inputs

        :param file: Source file to scan
        :return: Scan information dictionary
        """
        with open(file, 'r') as api_file:
            self.routes = {}
            self.stats = {'routes': 0, 'parsed': 0, 'filtered': 0, 'inputs': 0,
                          'misses': 0, 'safe': 0, 'unknown': 0, 'concerns': 0}
            self.server_name = ''
            self.candidates = []
            lines = api_file.readlines()
            if self.get_server_name(lines):
                self.analyse_lines(lines)
            else:
                utils.error_print('Server not found')
        self.stats['parsed'] = len(self.routes)
        info = {'routes': self.routes, 'stats': self.stats, 'date': datetime.now()}
        return info
