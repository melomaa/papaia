"""
Functions for collecting data for automated tests
"""
from os import path as os_path
import json
from papaia.reporter import write_output

GET_REQ = "GET"
POST_REQ = "POST"
PUT_REQ = "PUT"


def initialize_method(route, method, arg_data):
    """
    Initialize route and method if not already present in API data.

    :param route: API route
    :param method: API method
    :param arg_data: API data
    """
    if route not in arg_data:
        arg_data[route] = {}
    if method not in arg_data[route]:
        if method == GET_REQ:
            arg_data[route][method] = {'status': []}
        else:
            arg_data[route][method] = {'data': [], 'status': []}


def remove_obsolete(database, arg_data):
    """
    Remove unused routes from existing data

    :param database: Information database dictionary
    :param arg_data: Dictionary for found data
    """
    obsolete = []
    for route in arg_data.keys():
        if route not in database['routes']:
            obsolete.append(route)
    for route in obsolete:
        del arg_data[route]


def parse_database(database, arg_data):
    """
    Parse database for relevant information
    for Robot Framework

    :param database: Information database dictionary
    :param arg_data: Dictionary for found data
    """
    remove_obsolete(database, arg_data)
    for route in database['routes']:
        if GET_REQ in database['routes'][route]['methods']:
            initialize_method(route, GET_REQ, arg_data)
            for arg in database['routes'][route]['args']:
                if arg not in arg_data[route][GET_REQ]:
                    arg_data[route][GET_REQ][arg] = []
        if POST_REQ in database['routes'][route]['methods']:
            initialize_method(route, POST_REQ, arg_data)
        if PUT_REQ in database['routes'][route]['methods']:
            initialize_method(route, PUT_REQ, arg_data)


def create_robot_base(database, output_file):
    """
    Create / update configuration for Robot Framework tests

    :param database: Information database dictionary
    :param output_file: Name of the configuration file
    """
    arg_data = {}
    if output_file and os_path.exists(output_file):
        with open(output_file, 'r') as out_file:
            arg_data = json.load(out_file)
    parse_database(database, arg_data)
    output = json.dumps(arg_data, sort_keys=True, indent=4)
    write_output(output, output_file)
