Python API parser and input analyzer
====================================

A tool for parsing API routes for automated tests and
analyzing threats on malicious input. Currently works only with Flask framework.


### Configuration file

A configuration file can be specified with a command line parameter.
If no file is specified the tool looks for a default configuration file 
(called papaia_config.yaml) in the current directory.
The configuration file is in YAML format and it may provide following information:

  * List of functions used for validating input. These functions should be defined under KNOWN_FILTERS section.
  * List of methods used for validating input. These methods should be defined under KNOWN_CHECK_METHODS section.
  * List of functions that can handle unvalidated input. These functions should be defined under KNOWN_SAFE section.

Example configuration is provided at *samples/papaia_config.yaml*

Input analysis
--------------

Performs security scan on input data of requests.

The tool parses a given file, 
identifies API routes and related input data, and 
checks if input data is passed to an unsafe function without validation.

Example for running analysis and providing HTML report in a file:

    papaia -f html -o <output_file_path> <source_file>

#### Report

On the top of the report are statistics on the routes and inputs.
  * The first line shows how many routes were identified, 
how many were successfully parsed and how many have concerns related to input data
  * The second line shows how many inputs were detected, how many are safe / filtered
and how many may be open for vulnerabilities.

Below the statistics is a list of routes with detected concerns.

Parser
------

Provides a collection of API routes that receive input data.
The collection is stored into a JSON file.
By default each input has empty lists for test data and expected return codes.
These lists may populated for automated tests. Running the parser again will not remove the existing data.
