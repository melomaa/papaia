import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="papaia",
    version="0.2.0",
    author="Mikko Elomaa",
    author_email="",
    description="Python API parser and input analyzer.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/melomaa/papaia",
    packages=setuptools.find_packages(),
    package_data={"": ["*.html"]},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.6',
    entry_points={"console_scripts": "papaia = papaia.main:main"},
    install_requires=["Jinja2 >= 2.11.2", "Markdown >= 3.2.1", "PyYAML >= 5.3.1"],
)
