##  1. Input capture types

#### 1.1 request.args.get(<field>)
#### 1.2 reguest.query_string
#### 1.3 request.values
#### 1.4 reguest.form
#### 1.5 reguest.json
#### 1.6 reguset.get_data()
#### 1.7 request.get_json()

## 2. Threat types
#### 2.1 request input directly passed to function
#### 2.2 variable containing input passed to function
#### 2.3 input data used in format string before string is passed to a function

## 3. Validation types
#### 3.1 Input data is checked with common method such as isalnum, listed in config section KNOWN_CHECK_METHODS
#### 3.2 Input data is checked with designated filter, listed in config section KNOWN_FILTERS

## 4. Safe use
#### 4.1 Input is passed to known safe function, listed in config section KNOWN_SAFE
#### 4.2 Input is used after validation within correct indentation block
