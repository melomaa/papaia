papaia package
==============

Submodules
----------

papaia.api\_parser module
-------------------------

.. automodule:: papaia.api_parser
    :members:
    :undoc-members:
    :show-inheritance:

papaia.collector module
-----------------------

.. automodule:: papaia.collector
    :members:
    :undoc-members:
    :show-inheritance:

papaia.main module
------------------

.. automodule:: papaia.main
    :members:
    :undoc-members:
    :show-inheritance:

papaia.reporter module
----------------------

.. automodule:: papaia.reporter
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: papaia
    :members:
    :undoc-members:
    :show-inheritance:
