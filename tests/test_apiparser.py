import unittest
from mock import patch, mock_open, MagicMock
import api_parser
import data4test


class TestParser(unittest.TestCase):

    apiparser = None

    def setUp(self):
        self.apiparser = api_parser.ApiParser(data4test.TEST_CONFIG)
        self.apiparser.stats = {'routes': 0, 'filtered': 0, 'inputs': 0, 'misses': 0}

    def test_init_no_config(self):
        """
        Test initialization without configuration
        """
        no_conf_parser = api_parser.ApiParser()
        self.assertFalse(no_conf_parser.filter_config['KNOWN_CHECK_METHODS'])

    def test_init_partial_config(self):
        """
        Test initialization without configuration
        """
        test_config = {
            'KNOWN_FILTERS': ['some_filter', 'another'],
            'UNKNOWN_FIELD': [],
        }
        part_conf_parser = api_parser.ApiParser(test_config)
        self.assertListEqual(part_conf_parser.filter_config['KNOWN_CHECK_METHODS'], [])
        self.assertListEqual(part_conf_parser.filter_config['KNOWN_SAFE'], [])
        self.assertTrue('UNKNOWN_FIELD' not in part_conf_parser.filter_config)
        self.assertListEqual(part_conf_parser.filter_config['KNOWN_FILTERS'], test_config['KNOWN_FILTERS'])

    def test_check_for_function_unsafe_field(self):
        """
        Unsafe fields passed to unsafe function are threats
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = 'some_func(post_data["id"], no_field_var, post_data["title"])'
        input_var = {'name': 'post_data', 'desc': 'POST data',
                     'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [], 'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertTrue(input_var['name'] in self.apiparser.routes[test_path]['concerns'][0])
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['unknown'])
        # One safe and one unsafe field
        self.apiparser.routes[test_path] = {'concerns': []}
        input_var['safe_fields'] = [{'name': 'id'}]
        self.apiparser.check_for_function(test_line, input_var)
        self.assertTrue(input_var['name'] in self.apiparser.routes[test_path]['concerns'][0])
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['unknown'])

    def test_check_for_function_fields(self):
        """
        Safe fields passed to unsafe function is not a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = 'some_func(post_data["id"], stuff, post_data["title"])'
        input_var = {'name': 'post_data', 'desc': 'POST data',
                     'status': api_parser.VARIABLE_STATUS['threat'],
                     'safe_fields': [{'name': 'id'}, {'name': 'title'}],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.routes[test_path] = {'concerns': []}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['threat'])
        input_var['status'] = api_parser.VARIABLE_STATUS['unknown']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['unknown'])
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])

    def test_check_for_function_filter_field(self):
        """
        Fields passed to known filter appear in safe_fields list
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        filter_func = self.apiparser.filter_config['KNOWN_FILTERS'][0]
        test_line = filter_func + '(post_data["id"], post_data["title"], other_var)'
        input_var = {'name': 'post_data', 'desc': 'POST data', 'safe_fields': [{'name': 'id'}]}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertEqual(input_var['safe_fields'][1]['name'], 'title')
        self.assertEqual(input_var['safe_fields'][1]['indent'], 0)

        self.apiparser.routes[test_path] = {'concerns': []}

    def test_check_for_function_filter(self):
        """
        Variable passed to known filter added to list of safe variabled
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        filter_func = self.apiparser.filter_config['KNOWN_FILTERS'][0]
        test_line = '    if ' + filter_func + '(post_data["id"], something, post_data)'
        input_var = {'name': 'post_data', 'desc': 'POST data', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [{'name': 'id'}]}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        self.assertEqual(input_var['status'], 4)

    def test_check_for_function_safe(self):
        """
        Variable passed to known safe function is not a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        safe_func = self.apiparser.filter_config['KNOWN_SAFE'][0]
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = '    ' + safe_func + '(post_data["id"], stuff, post_data)'
        input_var = {'name': 'post_data', 'desc': 'POST data', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [{'name': 'id'}], 'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['unknown'])
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        # Threat detected previously
        input_var['status'] = api_parser.VARIABLE_STATUS['threat']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertEqual(input_var['status'], api_parser.VARIABLE_STATUS['threat'])
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])

    def test_check_for_function_unsafe(self):
        """
        Variable passed to unsafe function is a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        unsafe_func = 'unsafe_handler'
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = unsafe_func + '(post_data["id"], stuff, post_data)'
        input_var = {'name': 'post_data', 'desc': 'POST data',
                     'status': api_parser.VARIABLE_STATUS['unknown'], 'safe_fields': [{'name': 'id'}],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        expected_concern = ['POST data : (post_data) passed to **unsafe_handler**']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    def test_check_for_function_req(self):
        """
        Getting a member of dictionary is not a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "name = request.args.get('name', '')"
        input_var = {'name': 'name', 'desc': 'name', 'status': api_parser.VARIABLE_STATUS['unknown'], 'safe_fields': [], 'children': []}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])

    def test_check_for_function_format(self):
        """
        Variable passed to unsafe function is a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "print('{} gave no name or address'.format(safe_func(name)))"
        input_var = {'name': 'name', 'desc': 'name', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "print('{} gave no name or address'.format(name))"
        expected_concern = ['name : (name) passed to **.format**']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    def test_check_for_function_print(self):
        """
        Variable passed to unsafe function is a threat
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "print('He gave no name or address')"
        input_var = {'name': 'name', 'desc': 'name', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        test_line = "print('%s gave no name or address' % name)"
        expected_concern = ['name : (name) passed to **print**']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "print('%s gave no id or address: %s' % (name, another))"
        expected_concern = ['name : (name) passed to **print**']
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    def test_check_for_function_tuple(self):
        """
        Input variable as a member of a tuple in function parameters
        """
        test_path = 'test_path'
        test_var = 'name'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "    unsafe_func(arg1, (safe_func({}), arg2))".format(test_var)
        input_var = {'name': test_var, 'desc': 'GET arg', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        test_line = "    unsafe_func(arg1, (arg2, {}))".format(test_var)
        expected_concern = ['{} : ({}) passed to **unsafe_func**'.format(input_var['desc'], test_var)]
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    def test_check_for_function_nested(self):
        """
        Input variable passed to nested function
        """
        test_path = 'test_path'
        test_var = 'name'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        test_line = "    unsafe_func(arg1, safe_func({}))".format(test_var)
        input_var = {'name': test_var, 'desc': 'GET arg', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], [])
        test_line = "    safe_func(arg1, unsafe_func({}))".format(test_var)
        expected_concern = ['{} : ({}) passed to **unsafe_func**'.format(input_var['desc'], test_var)]
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    def test_check_for_function_nested_triple(self):
        """
        Nested multiple times
        """
        test_path = 'test_path'
        test_var = 'name'
        self.apiparser.path = test_path
        self.apiparser.routes[test_path] = {'concerns': []}
        input_var = {'name': test_var, 'desc': 'GET arg', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        test_line = "    safe_func(arg1, unknown_func(unsafe_func({}), another_var))".format(test_var)
        expected_concern = ['{} : ({}) passed to **unsafe_func**'.format(input_var['desc'], test_var)]
        self.apiparser.check_for_function(test_line, input_var)
        self.assertListEqual(self.apiparser.routes[test_path]['concerns'], expected_concern)

    @patch('papaia.utils.get_func_and_params')
    def test_check_for_function_no_match(self, get_mock):
        """
        Variable is not detected as function parameter
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        test_line = 'some_func(lorem ipsum)'
        input_var = {'name': 'post_data', 'desc': 'POST data', 'safe_fields': [{'name': 'id'}]}
        get_mock.return_value = ('', [], len(test_line))
        self.apiparser.check_for_function(test_line, input_var)
        self.assertEqual(get_mock.call_count, 1)

    def test_check_for_assignment(self):
        """
        Input variable is assigned to another variable
        """
        self.apiparser.candidates = []
        self.apiparser.add_candidate('post_data', 'POST data')
        self.apiparser.add_candidate('req_args', 'GET args')
        test_var = 'some_var'
        # POST data
        input_var = self.apiparser.candidates[0]
        test_line = test_var + ' = ' + input_var['name']
        expected_item = {'name': test_var, 'desc': 'POST data', 'status': api_parser.VARIABLE_STATUS['unknown'],
                         'safe_fields': [], 'children': [], 'parent': input_var['name'],
                         'stats': data4test.EMPTY_VAR_STATS}
        self.apiparser.check_for_assignment(test_line, input_var)
        self.assertDictEqual(self.apiparser.candidates[-1], expected_item)
        self.assertTrue(test_var in input_var['children'])
        # GET args
        input_var = self.apiparser.candidates[1]
        test_var = 'get_var'
        test_line = test_var + ' = ' + input_var['name'] + '.get("name", "")'
        expected_item = {'name': test_var, 'desc': 'GET args', 'status': api_parser.VARIABLE_STATUS['unknown'],
                         'safe_fields': [], 'children': [], 'parent': input_var['name'],
                         'stats': data4test.EMPTY_VAR_STATS}
        self.apiparser.check_for_assignment(test_line, input_var)
        self.assertEqual(len(self.apiparser.candidates), 4)
        self.assertDictEqual(self.apiparser.candidates[-1], expected_item)
        self.assertTrue(test_var in input_var['children'])

    def test_check_for_assignment_field(self):
        """
        Field of an input variable is assigned to another variable
        """
        input_var = {'name': 'post_data', 'desc': 'POST data', 'status': api_parser.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': []}
        test_var = 'some_var'
        test_line = test_var + ' = ' + input_var['name'] + "['key']"
        self.apiparser.candidates = []
        expected_item = {'name': test_var, 'desc': 'POST data', 'status': api_parser.VARIABLE_STATUS['unknown'],
                         'safe_fields': [], 'children': [], 'parent': input_var['name'],
                         'stats': data4test.EMPTY_VAR_STATS}
        self.apiparser.check_for_assignment(test_line, input_var)
        self.assertDictEqual(self.apiparser.candidates[0], expected_item)

    def test_check_for_assignment_no_match(self):
        """
        No assignment on the line
        """
        input_var = {'name': 'post_data', 'desc': 'POST data'}
        test_line = 'no_assignment(' + input_var['name'] + ')'
        self.apiparser.candidates = []
        self.apiparser.check_for_assignment(test_line, input_var)
        self.assertEqual(self.apiparser.candidates, [])

    def test_analyze_candidates(self):
        """
        Check set of variables against test source lines
          - Test passing variable to safe function
          - Test assigning to another variable and passing that to unsafe function
          - Test validating multiple fields with different methods on one line
          - Test validating variable
          - Test validating multiple fields with same method on one line
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.candidates = []
        self.apiparser.add_candidate('post_data', 'POST data')
        self.apiparser.add_candidate('uid', 'GET data')
        self.apiparser.add_candidate('json_data', 'POST data')
        self.apiparser.add_candidate('request.json', 'POST data')
        self.apiparser.add_candidate('req_data', 'POST data')
        self.apiparser.routes[test_path] = {'concerns': []}
        threat_var = 'some_var'
        expected_threats = ['POST data : (' + threat_var + ') passed to **unsafe_func**',
                            'POST data : (req_data) passed to **unsafe_func**']
        expected_status = {'post_data': 4, 'uid': -1, 'json_data': -1,
                           'some_var': -1, 'req_data': -1, 'request.json': -1}
        for test_line in data4test.TEST_ANALYZE_LINES:
            self.apiparser.analyze_candidates(test_line)
        self.assertEqual(len(self.apiparser.routes[test_path]['concerns']), len(expected_threats))
        for item in expected_threats:
            self.assertTrue(item in self.apiparser.routes[test_path]['concerns'])
        self.assertEqual(len(self.apiparser.candidates), 6)
        for item in self.apiparser.candidates:
            self.assertEqual(item['status'], expected_status[item['name']])
            if item['name'] == 'json_data':
                self.assertTrue({'name': 'id', 'indent': 8} in item['safe_fields'])
                self.assertTrue({'name': 'name', 'indent': 8} in item['safe_fields'])

    def test_analyze_candidates_assign_safe(self):
        """
        Test that passing assigned variable to known filter does not count as filtered in stats
        """
        test_path = 'test_path'
        self.apiparser.path = test_path
        self.apiparser.stats = data4test.EMPTY_STATS.copy()
        self.apiparser.candidates = []
        test_var = 'post_data'
        self.apiparser.add_candidate(test_var, 'POST data')
        test_lines = ['    some_var = post_data', '    if ' + data4test.FILTER_FUNC + '(some_var):']
        for test_line in test_lines:
            self.apiparser.analyze_candidates(test_line)
        self.assertEqual(len(self.apiparser.candidates), 2)
        self.assertEqual(self.apiparser.candidates[0]['name'], test_var)
        self.assertEqual(self.apiparser.candidates[0]['status'], -1)
        self.assertEqual(self.apiparser.candidates[1]['name'], 'some_var')
        self.assertEqual(self.apiparser.candidates[1]['status'], 4)

    def test_parse_api_line_get_post(self):
        """
        Test that GET method is used if API decorator does not specify methods
        """
        self.apiparser.stats = data4test.EMPTY_STATS.copy()
        self.apiparser.server_name = 'server'
        test_index = 63
        test_path = '/login'
        test_methods = "'GET', 'POST'"
        test_line = '@server.route( "{}" methods=[{}])'.format(test_path, test_methods)
        expected_route = {'methods': ['GET', 'POST'], 'line': test_index,
                          'args': [], 'concerns': []}
        self.apiparser.parse_api_line(test_line, test_index)
        self.assertEqual(self.apiparser.stats['routes'], 1)
        self.assertDictEqual(self.apiparser.routes[test_path], expected_route)

    def test_parse_api_line_default(self):
        """
        Test that GET method is used if API decorator does not specify methods
        """
        self.apiparser.stats = data4test.EMPTY_STATS.copy()
        self.apiparser.server_name = 'server'
        test_index = 63
        test_path = '/login'
        test_line = '@server.route("' + test_path + '")'
        expected_route = {'methods': ['GET'], 'line': test_index,
                          'args': [], 'concerns': []}
        self.apiparser.parse_api_line(test_line, test_index)
        self.assertListEqual(self.apiparser.routes[test_path]['methods'], ['GET'])
        self.assertEqual(self.apiparser.stats['routes'], 1)
        self.assertDictEqual(self.apiparser.routes[test_path], expected_route)

    def test_parse_api_line_invalid(self):
        """
        Verify correct handling of incomplete API decorator
        """
        index = 10
        test_line = '@server.route'
        self.apiparser.stats = data4test.EMPTY_STATS.copy()
        self.apiparser.routes = []
        self.apiparser.server_name = 'server'
        path = self.apiparser.parse_api_line(test_line, index)
        self.assertEqual(path, '')
        self.assertEqual(self.apiparser.stats['routes'], 1)
        test_line = '@server.route("/test", methods=)'
        path = self.apiparser.parse_api_line(test_line, index)
        self.assertEqual(path, '')
        self.assertEqual(self.apiparser.stats['routes'], 2)
        self.assertEqual(len(self.apiparser.routes), 0)

    def test_path_post_process(self):
        """
        Test statistics handling after API path has been analyzed
        """
        self.apiparser.stats = data4test.EMPTY_STATS.copy()
        self.apiparser.candidates = [{'status': api_parser.VARIABLE_STATUS['unknown'], 'children': [],
                                      'name': 'abc', 'stats': data4test.EMPTY_VAR_STATS.copy()},
                                     {'status': api_parser.VARIABLE_STATUS['unknown'], 'children': ['name', 'nobody'],
                                      'name': 'par', 'stats': {'filtered': 1, 'threats': 0, 'safe': 0}},
                                     {'status': api_parser.VARIABLE_STATUS['unknown'], 'children': [], 'parent': 'par',
                                      'name': 'name', 'stats': {'filtered': 0, 'threats': 0, 'safe': 1}},
                                     {'status': api_parser.VARIABLE_STATUS['unknown'], 'children': [],
                                      'name': 'def', 'stats': data4test.EMPTY_VAR_STATS.copy()},
                                     {'status': api_parser.VARIABLE_STATUS['unknown'], 'children': [],
                                      'name': 'threat', 'stats': {'filtered': 0, 'threats': 1, 'safe': 0}},
                                     {'status': api_parser.VARIABLE_STATUS['unknown'], 'children': [],
                                      'name': 'safe_one', 'stats': {'filtered': 0, 'threats': 0, 'safe': 1}},
                                     ]
        self.apiparser.path_post_process()
        self.assertEqual(self.apiparser.stats['unknown'], 2)
        self.assertEqual(self.apiparser.stats['safe'], 1)
        self.assertEqual(self.apiparser.stats['filtered'], 1)

    def test_get_server_name_short(self):
        """
        Test detection of server name
        """
        self.apiparser.server_name = ''
        test_lines = [
            'from flask import Flask',
            '',
            'app = Flask(__name__)',
            '',
            ]
        status = self.apiparser.get_server_name(test_lines)
        self.assertTrue(status)
        self.assertEqual(self.apiparser.server_name, 'app')

    def test_get_server_name(self):
        """
        Test detection of server name
        """
        self.apiparser.server_name = ''
        test_lines = [
            'import flask',
            '',
            'server = flask.Flask(__name__)',
            '',
            ]
        status = self.apiparser.get_server_name(test_lines)
        self.assertTrue(status)
        self.assertEqual(self.apiparser.server_name, 'server')

    def test_get_server_name_invalid(self):
        """
        Test detection of server name
        """
        self.apiparser.server_name = ''
        test_lines = [
            'import flask',
            '',
            'server;name = flask.Flask(__name__)',
            '',
            ]
        status = self.apiparser.get_server_name(test_lines)
        self.assertFalse(status)
        self.assertEqual(self.apiparser.server_name, '')

    @patch('api_parser.ApiParser.analyze_candidates')
    def test_find_calls(self, analyze_mock):
        """
        Test finding of API functions and calling of analysis for input variables
        """
        self.apiparser.candidates = []
        with patch("builtins.open", mock_open(read_data='\n'.join(data4test.TEST_FILE.splitlines()))) as mock_file:
            dummy_file = 'test_file'
            analyze_mock.return_value = None
            result = self.apiparser.find_calls(dummy_file)
            mock_file.assert_called_with(dummy_file, 'r')
            # self.assertEqual(analyze_mock.call_count, 9)
            self.assertEqual(result['stats']['routes'], 4)
            self.assertEqual(result['stats']['parsed'], 3)
            self.assertEqual(result['stats']['inputs'], 3)
            self.assertListEqual(result['routes']['/control']['methods'], ['POST'])
            self.assertEqual(result['routes']['/control']['line'], 4)
            self.assertListEqual(result['routes']['/data']['methods'], ['GET', 'PUT'])
            self.assertEqual(result['routes']['/data']['line'], 12)
            self.assertListEqual(result['routes']['/switch']['methods'], ['POST'])
            self.assertEqual(result['routes']['/switch']['line'], 18)
            self.assertListEqual(self.apiparser.candidates, [])

    @patch('papaia.utils.error_print')
    @patch('api_parser.ApiParser.analyze_candidates')
    def test_find_calls_no_server(self, analyze_mock, err_mock):
        """
        Check correct behaviour when server line is not detected
        """
        self.apiparser.candidates = []
        with patch("builtins.open", mock_open(read_data='file without server line')) as mock_file:
            dummy_file = 'test_file'
            self.apiparser.find_calls(dummy_file)
            self.assertEqual(analyze_mock.call_count, 0)
            self.assertEqual(mock_file.call_count, 1)
            err_mock.assert_called_with('Server not found')


if __name__ == '__main__':
    unittest.main()
