import unittest
from mock import patch, sentinel, mock_open
import json
import papaia.collector as collector


class TestCollector(unittest.TestCase):
    def test_initialize_method(self):
        test_data = {}
        test_route = '/test'
        test_method = 'GET'
        collector.initialize_method(test_route, test_method, test_data)
        self.assertListEqual(test_data[test_route][test_method]['status'], [])
        self.assertTrue('data' not in test_data[test_route][test_method])
        test_method = 'PUT'
        collector.initialize_method(test_route, test_method, test_data)
        self.assertListEqual(test_data[test_route][test_method]['status'], [])
        self.assertTrue('data' in test_data[test_route][test_method])
        first_data = {"name": "first test"}
        test_data[test_route][test_method]['data'].append(first_data)
        collector.initialize_method(test_route, test_method, test_data)
        self.assertListEqual(test_data[test_route][test_method]['data'], [first_data])

    def test_remove_obsolete(self):
        test_route = '/test'
        route_two = '/two'
        route_three = '/three'
        extra_route = '/extra'
        test_base = {'routes': {test_route: {}, route_two: {}, route_three: {}}}
        test_data = {test_route: {}, extra_route: {}, route_two: {}}
        expected = {test_route: {}, route_two: {}}
        collector.remove_obsolete(test_base, test_data)
        self.assertDictEqual(test_data, expected)

    @patch('papaia.collector.remove_obsolete')
    def test_parse_database(self, rm_mock):
        test_route = '/test'
        route_two = '/two'
        route_three = '/three'
        test_base = {'routes': {test_route: {'args': [], 'methods': ['POST']},
                                route_two: {'args': ['uid'], 'methods': ['GET', 'POST']},
                                route_three: {'args': [], 'methods': ['PUT']}}}
        test_data = {}
        expected = {'/test': {'POST': {'data': [], 'status': []}},
                    '/two': {'GET': {'status': [], 'uid': []}, 'POST': {'data': [], 'status': []}},
                    '/three': {'PUT': {'data': [], 'status': []}}}
        collector.parse_database(test_base, test_data)
        self.assertDictEqual(test_data, expected)
        second_base = {'routes': {route_two: {'args': ['uid', 'name'], 'methods': ['GET', 'POST']}}}
        expected[route_two]['GET']['name'] = []
        collector.parse_database(second_base, test_data)
        self.assertDictEqual(test_data, expected)

    @patch('builtins.print')
    @patch('papaia.collector.parse_database')
    def test_create_robot_base_no_file(self, parse_mock, print_mock):
        test_base = sentinel.database
        collector.create_robot_base(test_base, None)
        parse_mock.assert_called_with(sentinel.database, {})
        print_mock.assert_called_with('{}')

    @patch('papaia.collector.os_path.exists')
    @patch('papaia.collector.parse_database')
    def test_create_robot_base(self, parse_mock, path_mock):
        test_base = sentinel.database
        test_file = 'test_out_file.json'
        test_data = {'/test': {'POST': {'data': [], 'status': []}}}
        expected = json.dumps(test_data, sort_keys=True, indent=4)
        path_mock.return_value = True
        with patch('builtins.open', mock_open(read_data=json.dumps(test_data))) as file_mock:
            # print(json.load(file_mock))
            assert open('/tmp').read() == json.dumps(test_data)
            collector.create_robot_base(test_base, test_file)
        self.assertEqual(file_mock().write.call_args.args[0], expected)
        parse_mock.assert_called_with(sentinel.database, test_data)


if __name__ == '__main__':
    unittest.main()
