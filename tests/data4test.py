EMPTY_STATS = {'routes': 0, 'filtered': 0, 'inputs': 0,
               'misses': 0, 'safe': 0, 'unknown': 0}
EMPTY_VAR_STATS = {'filtered': 0, 'threats': 0, 'safe': 0}

#################################################
# Test file content for iterating through lines #
#################################################
TEST_FILE = """
server = Flask(__name__)

@server.route("/control", methods=['POST'])
def control():
    post_data = request.get_data()
    # comment_func(post_data)
    skip_func(post_data)  #  api_parser: skip (for some reason)
    unsafe_func(post_data)
    return {}

@server.route("/data", methods=['GET', 'PUT'])
def handle_data():
    json_var = request.get_json()
    unsafe_func(json_var)
    return {}

@server.route('/switch', methods=["POST"])
def api_switch():
    if request.json['country'].isalnum():
        if request.json['enabled']:
            do_switch('up', request.json['country'])
        else:
            do_switch('down', request.json['country'])
    return jsonify(request.json), 201

@server.route("/test")  # api_parser: skip
def test_func():
    id = request.args.get('id', '')
    return 200
"""

############################################################
# Test data for detecting input validation in IF statement #
############################################################
TEST_IF_VALIDATION = [
    {'line': '    if something and request.json["name"].isalnum() and request.json["id"]:', 'name': 'request.json'},
    {'line': '    if request.form["field"].isalnum():', 'name': 'request.form'},
    {'line': '    if request.args.get("field", "").isnumeric():', 'name': 'request.args.get'},
    {'line': '    if request.get_json()["field"].isalnum():', 'name': 'request.get_json'},
    {'line': '    if request.get_data()["field"].isalnum():', 'name': 'request.get_data'},
    {'line': '    if request.length < 100', 'name': 'request.length'}
    ]

######################################################################
# Test data for detecting use of request input as function parameter #
######################################################################
TEST_INPUT_FUNC = [
    {'line': 'func(variable, request.args.get("field", ""))',
     'fields': [{'name': 'field'}], 'names': ['request.args.get']},
    {'line': '    func(request.query_string, var)', 'fields': [], 'names': ['request.query_string']},
    {'line': 'func(request.args, var)', 'fields': [], 'names': ['request.args']},
    {'line': "func(variable, request.get_json()['field'], variable)", 'fields': [{'name': 'field'}], 'names': ['request.get_json']},
    {'line': 'func(variable, request.get_data())', 'fields': [], 'names': ['request.get_data']},
    {'line': "func(variable, request.json['field'], variable)", 'fields': [{'name': 'field'}], 'names': ['request.json']},
    {'line': 'func(variable, request.form["field"])', 'fields': [{'name': 'field'}], 'names': ['request.form']}
]

##############################################
# Single and multiline IF statement handling #
##############################################
MULTI_IF = [
    '    if something and \\',
    '            (otherthing or request.json["name"]',
    '            and enabled) and finalthing:  # Long if statement',
    '        This is inside if'
]

FAIL_IF = [
    '    if something and \\',
    '            (otherthing or request.json["name"]) \\',
    '            and finalthing  # Long if statement',
    '        This is inside if',
    '        But it is missing the colon',
]


SINGLE_IF = [
    '    if something and other:',
    '        This is inside if'
]

##################################################
# Test data for handling multiline function call #
##################################################
MULTILINE_FUNC = [
    {'lines': [
        '    long_func_name(first, second,',
        '                   some_nested_func_call(),',
        '                   third, fourth)'
        ], 'length': 73, 'offset': 2},
    {'lines': [
        '    long_func_name(first, second, some_nested_func_call())',
    ], 'length': 58, 'offset': 0},
    {'lines': [
        '    long_func_name(first, second,',
        '                   some_nested_func_call(),',
        '                   another_func(arg1, arg 2)',
        '                   SOME_CONSTANT,',
        '                   third, fourth)'
    ], 'length': 114, 'offset': 4},
    {'lines': [
        '    long_func_name(first, second,',
        '                   some_nested_func_call(),',
        '                   another_func(arg1, arg 2)',
        '                   SOME_CONSTANT,',
        '                   third, fourth,',
        '                   TOO_LONG)'
    ], 'length': 33, 'offset': 0},
    {'lines': [
        '    long_func_name(first, second,',
        '                   some_nested_func_call(),',
        '                   another_func(arg1, arg 2)',
        '                   SOME_CONSTANT,',
        '                   third, fourth,'
    ], 'length': 33, 'offset': 0},
]

SAFE_FUNC = 'safe_func'
UNSAFE_FUNC = 'unsafe_func'
FILTER_FUNC = 'whitelist_input'

########################
# Analyze set of lines #
########################
TEST_ANALYZE_LINES = [
    '    ' + SAFE_FUNC + '(post_data)',
    '    some_var = post_data',
    '    ' + UNSAFE_FUNC + '(some_var)',
    '    if {}(request.json[\'id\']) and {}(req_data[\'name\']):'.format(SAFE_FUNC, UNSAFE_FUNC),
    '    if post_data[\'id\'].isnumeric() and post_data[\'name\'].isalnum():',
    '        ' + UNSAFE_FUNC + '(post_data[\'id\'], post_data[\'name\'])',
    '    ' + FILTER_FUNC + '(post_data)',
    '        if uid.isalnum():',
    '            ' + UNSAFE_FUNC + '(uid)',
    '        if json_data[\'id\'].isalnum() and json_data[\'name\'].isalnum():',
    '            ' + UNSAFE_FUNC + '(json_data[\'id\'], json_data[\'name\'])',
]

######################
# Test configuration #
######################
TEST_CONFIG = {
    'KNOWN_FILTERS': [FILTER_FUNC],
    'KNOWN_CHECK_METHODS': ['isnumeric', 'isalnum'],
    'KNOWN_SAFE': [SAFE_FUNC]
}