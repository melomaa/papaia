Papaia testing
--------------

Generic test data is stored at data4test.py

### Unit tests
Files starting with ***test_*** contain unit tests. 
All unit tests can be run using the suite_runner.py

    PYTHONPATH=./:./papaia python3 tests/suite_runner.py

The suite runner uses coverage module to provide test coverage information. 
By default an XML report is produced and stored into a file called tests/papaia-coverage.xml, 
which can be passed to an analyzer (such as SonarQube) for visualization.

### Functional tests
Functional tests for parser can be run with

    PYTHONPATH=./:./papaia python3 tests/func_test_parser.py

The code lines to examine are provided by negative_cases.py