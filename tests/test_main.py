import unittest
from mock import patch, mock_open, MagicMock, sentinel, PropertyMock
from mock import DEFAULT as mock_DEFAULT
import argparse
import main

TEST_CONFIG = """
TEST_KEY:
   - First item
   - Second item
"""
EXPECTED_CONFIG = {'TEST_KEY': ['First item', 'Second item']}


class TestMain(unittest.TestCase):

    def test_get_configuration(self):
        test_file = 'test_file'
        with patch("builtins.open", mock_open(read_data=TEST_CONFIG)) as mock_file:
            mock_file.side_effect = [mock_DEFAULT]
            config = main.get_configuration(test_file, False)
            self.assertDictEqual(config, EXPECTED_CONFIG)

    def test_get_configuration_not_found(self):
        test_file = 'test_file'
        with patch("builtins.open", mock_open(read_data=TEST_CONFIG)) as mock_file:
            mock_file.side_effect = [FileNotFoundError]
            self.assertRaises(RuntimeError, main.get_configuration, test_file, False)

    @patch('builtins.print')
    def test_get_configuration_exp_default(self, print_mock):
        with patch("builtins.open", mock_open(read_data=TEST_CONFIG)) as mock_file:
            mock_file.side_effect = [OSError('Test OS Exception')]
            config = main.get_configuration(main.DEFAULT_CONFIG, False)
            self.assertTrue(config is None)
            print_mock.assert_called_with('\n!!!! No configuration file available for filters.  !!!!\n')

    @patch('builtins.print')
    def test_get_configuration_not_found_test(self, print_mock):
        with patch("builtins.open", mock_open(read_data=TEST_CONFIG)) as mock_file:
            mock_file.side_effect = [FileNotFoundError('Test Exception')]
            config = main.get_configuration(main.DEFAULT_CONFIG, True)
            self.assertTrue(config is None)
            self.assertEqual(print_mock.call_count, 0)

    def test_init_argparse(self):
        parser = main.init_argparse()
        self.assertTrue(isinstance(parser, argparse.ArgumentParser))

    def test_check_argument_safe(self):
        test_data = [('safe/file_name.ext', True),
                     ('two-parts', True),
                     ('file; cmd', False),
                     ('$var', False),
                     ('valid && invalid', False),
                     ('#some_char', False),
                     ]
        for item in test_data:
            self.assertEqual(main.check_argument_safe(item[0]), item[1])
        self.assertEqual(main.check_argument_safe(123), False)
        self.assertEqual(main.check_argument_safe({'key': 'value'}), False)

    @patch('main.error_print')
    @patch('main.output_database')
    @patch('main.create_robot_base')
    @patch('main.ApiParser')
    @patch('main.get_configuration')
    @patch('main.init_argparse')
    def test_main_no_config_test(self, init_mock, cfg_mock, api_mock, robot_mock, out_mock, err_mock):
        source_file = 'api_source.py'
        mock_parser = MagicMock()
        mock_args = MagicMock()
        mock_args.config = False
        mock_args.files = [source_file]
        mock_args.test = True
        mock_args.output = 'bad; output & path'
        mock_parser.parse_args.return_value = mock_args
        init_mock.return_value = mock_parser
        cfg_mock.return_value = sentinel.config
        apiparser_inst = api_mock.return_value
        apiparser_inst.find_calls.return_value = {}
        main.main()
        cfg_mock.assert_called_with(main.DEFAULT_CONFIG, mock_args.test)
        api_mock.assert_called_with(sentinel.config)
        apiparser_inst.find_calls.assert_called_with(source_file)
        err_mock.assert_called_with(main.MALFORMED_OUT)
        robot_mock.assert_called_with({}, None)
        self.assertEqual(out_mock.call_count, 0)

    @patch('main.output_database')
    @patch('main.create_robot_base')
    @patch('main.ApiParser')
    @patch('main.get_configuration')
    @patch('main.init_argparse')
    def test_main_config_no_test(self, init_mock, cfg_mock, api_mock, robot_mock, out_mock):
        test_config_file = 'test_cfg_file'
        source_file = 'api_source.py'
        mock_parser = MagicMock()
        mock_args = MagicMock()
        mock_args.config = test_config_file
        mock_args.files = [source_file]
        mock_args.test = False
        mock_args.format = 'html'
        test_output = 'valid/report_file.md'
        mock_args.output = test_output
        mock_parser.parse_args.return_value = mock_args
        init_mock.return_value = mock_parser
        cfg_mock.return_value = sentinel.config
        apiparser_inst = api_mock.return_value
        apiparser_inst.find_calls.return_value = {}
        main.main()
        cfg_mock.assert_called_with(test_config_file, mock_args.test)
        api_mock.assert_called_with(sentinel.config)
        apiparser_inst.find_calls.assert_called_with(source_file)
        self.assertEqual(robot_mock.call_count, 0)
        self.assertEqual(out_mock.call_args.args[0][source_file], {})
        self.assertEqual(out_mock.call_args.kwargs['out_format'], 'html')
        self.assertEqual(out_mock.call_args.kwargs['output_file'], test_output)

    @patch('main.error_print')
    @patch('main.output_database')
    @patch('main.create_robot_base')
    @patch('main.ApiParser')
    @patch('main.get_configuration')
    @patch('main.init_argparse')
    def test_main_config_cfg_exp(self, init_mock, cfg_mock, api_mock, robot_mock, out_mock, err_mock):
        test_config_file = 'test_cfg_file'
        source_file = 'api_source.py'
        mock_parser = MagicMock()
        mock_args = MagicMock()
        mock_args.config = test_config_file
        mock_args.files = [source_file]
        mock_args.test = False
        mock_parser.parse_args.return_value = mock_args
        init_mock.return_value = mock_parser
        cfg_mock.side_effect = RuntimeError('Test exception')
        main.main()
        cfg_mock.assert_called_with(test_config_file, mock_args.test)
        self.assertEqual(api_mock.call_count, 0)
        self.assertEqual(robot_mock.call_count, 0)
        self.assertEqual(out_mock.call_count, 0)
        err_mock.assert_called_with('Test exception')

    @patch('main.error_print')
    @patch('main.check_argument_safe')
    @patch('main.ApiParser')
    @patch('main.get_configuration')
    @patch('main.init_argparse')
    def test_main_config_file_exp(self, init_mock, cfg_mock, api_mock, safe_mock, err_mock):
        source_file = 'api_source.py'
        mock_parser = MagicMock()
        mock_args = MagicMock()
        mock_args.config = None
        mock_args.files = [source_file]
        mock_args.test = False
        mock_parser.parse_args.return_value = mock_args
        init_mock.return_value = mock_parser
        cfg_mock.return_value = sentinel.config
        safe_mock.return_value = False
        main.main()
        cfg_mock.assert_called_with(main.DEFAULT_CONFIG, mock_args.test)
        self.assertEqual(api_mock.call_count, 1)
        err_mock.assert_called_with(main.MALFORMED_SRC)


if __name__ == '__main__':
    unittest.main()
