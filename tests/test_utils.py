import unittest
from mock import patch, mock_open, MagicMock
import data4test
import papaia.utils as utils


class TestStatic(unittest.TestCase):

    def test_field_is_safe(self):
        test_field = 'two'
        mock_match = MagicMock()
        mock_match.groups.return_value = [1]
        mock_match.group.return_value = test_field
        test_var = {'safe_fields': []}
        result = utils.field_is_safe(mock_match, test_var)
        self.assertFalse(result)
        mock_match.groups.return_value = [1, 2]
        test_var = {'safe_fields': []}
        result = utils.field_is_safe(mock_match, test_var)
        self.assertFalse(result)
        test_var = {'safe_fields': [{'name': 'none'}]}
        result = utils.field_is_safe(mock_match, test_var)
        self.assertFalse(result)
        test_var = {'safe_fields': [{'name': test_field}]}
        result = utils.field_is_safe(mock_match, test_var)
        self.assertTrue(result)

    def test_variable_is_safe(self):
        test_status = utils.VARIABLE_STATUS['unknown']
        test_indent = 8
        result = utils.variable_is_safe(test_status, test_indent)
        self.assertFalse(result)
        test_status = 4
        result = utils.variable_is_safe(test_status, test_indent)
        self.assertTrue(result)
        test_indent = 4
        result = utils.variable_is_safe(test_status, test_indent)
        self.assertFalse(result)

    def test_update_variable_status(self):
        test_var = {'status': utils.VARIABLE_STATUS['unknown']}
        test_status = 8
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], test_status)
        test_status = utils.VARIABLE_STATUS['unknown']
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], test_status)
        test_status = utils.VARIABLE_STATUS['threat']
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], test_status)

    def test_update_variable_status_not(self):
        test_var = {'status': utils.VARIABLE_STATUS['threat']}
        test_status = 8
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], utils.VARIABLE_STATUS['threat'])
        test_status = utils.VARIABLE_STATUS['unknown']
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], utils.VARIABLE_STATUS['threat'])
        test_var['status'] = 4
        test_status = 8
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], 4)
        test_status = utils.VARIABLE_STATUS['safe']
        utils.update_variable_status(test_var, test_status)
        self.assertEqual(test_var['status'], 4)

    @patch('papaia.utils.get_indent')
    def test_handle_known_filter_full(self, ind_mock):
        test_var = 'variab'
        test_indent = '        '
        test_line = test_indent + 'if my_filter(abc, {})'.format(test_var)
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown']}
        test_params = ['abc', test_var]
        ind_mock.return_value = len(test_indent)
        utils.handle_known_filter(test_line, input_var, test_params)
        self.assertEqual(input_var['status'], len(test_indent))

    @patch('papaia.utils.get_indent')
    def test_handle_known_filter_field(self, ind_mock):
        test_var = 'variab'
        test_field = 'field'
        test_indent = '        '
        test_line = test_indent + 'if my_filter(abc, {}["{}"])'.format(test_var, test_field)
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'], 'safe_fields': []}
        test_params = ['abc', '{}["{}"]'.format(test_var, test_field)]
        ind_mock.return_value = len(test_indent)
        add_mock = MagicMock(side_effect=utils.add_safe_field)
        utils.add_safe_field = add_mock
        utils.handle_known_filter(test_line, input_var, test_params)
        self.assertEqual(add_mock.call_count, 1)
        self.assertDictEqual(input_var['safe_fields'][0], {'name': test_field, 'indent': len(test_indent)})
        utils.handle_known_filter(test_line, input_var, test_params)
        self.assertEqual(add_mock.call_count, 1)

    def test_check_parameter_use_noparam(self):
        test_var = 'variab'
        test_func = 'my_func'
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'], 'safe_fields': []}
        test_indent = '        '
        test_line = test_indent + ''
        test_params = []
        test_filter_config = {}
        result = utils.check_parameter_use(test_line, test_func, input_var, test_params, test_filter_config)
        self.assertTrue(result)

    @patch('papaia.utils.handle_known_filter')
    def test_check_parameter_use_filter(self, filter_mock):
        test_var = 'variab'
        test_func = 'my_func'
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'], 'safe_fields': []}
        test_indent = '        '
        test_line = test_indent + test_func + '(variab)'
        test_params = [test_var]
        test_filter_config = {'KNOWN_FILTERS': [test_func]}
        result = utils.check_parameter_use(test_line, test_func, input_var, test_params, test_filter_config)
        self.assertTrue(result)
        filter_mock.assert_called_with(test_line, input_var, test_params)

    def test_check_parameter_use_safe(self):
        test_var = 'variab'
        test_func = 'my_func'
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'], 'safe_fields': [],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        test_indent = '        '
        test_line = test_indent + test_func + '(variab)'
        test_params = [test_var]
        test_filter_config = {'KNOWN_SAFE': [test_func]}
        result = utils.check_parameter_use(test_line, test_func, input_var, test_params, test_filter_config)
        self.assertTrue(result)

    @patch('papaia.utils.check_safe_fields')
    def test_check_parameter_use_field(self, field_mock):
        test_var = 'variab'
        test_func = 'my_func'
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': ["something"],
                     'stats': data4test.EMPTY_VAR_STATS.copy()}
        test_indent = '        '
        test_line = test_indent + test_func + '(variab)'
        test_params = [test_var + "['field']"]
        test_filter_config = {}
        field_mock.return_value = True
        result = utils.check_parameter_use(test_line, test_func, input_var, test_params, test_filter_config)
        self.assertTrue(result)

    def test_check_parameter_use_unsafe(self):
        test_var = 'variab'
        test_func = 'my_func'
        input_var = {'name': test_var, 'status': utils.VARIABLE_STATUS['unknown'], 'safe_fields': []}
        test_indent = '        '
        test_line = test_indent + test_func + '(variab)'
        test_params = [test_var]
        test_filter_config = {}
        result = utils.check_parameter_use(test_line, test_func, input_var, test_params, test_filter_config)
        self.assertFalse(result)

    def test_method_full(self):
        """
        Test filter method detection on input variable
        """
        test_var = 'variab'
        test_indent = '    '
        test_line = test_indent + 'if something and ' + test_var + '.isnumeric():'
        input_var = {'name': test_var, 'desc': 'Form data', 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': []}
        result = utils.check_for_method(test_line, input_var, data4test.TEST_CONFIG['KNOWN_CHECK_METHODS'])
        self.assertTrue(result)
        self.assertEqual(input_var['status'], len(test_indent))

    def test_method_field(self):
        """
        Test filter method detection on field of request input variable
        """
        test_indent = '    '
        test_line = test_indent + 'if something and request.json["name"].isalnum() and request.json["id"]:'
        input_var = {'name': 'request.json', 'desc': 'Form data', 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': []}
        result = utils.check_for_method(test_line, input_var, data4test.TEST_CONFIG['KNOWN_CHECK_METHODS'])
        self.assertTrue(result)
        self.assertEqual(len(input_var['safe_fields']), 1)
        self.assertDictEqual(input_var['safe_fields'][0], {'name': 'name', 'indent': 4})

    def test_method_none(self):
        """
        Test filter method detection on input variable
        """
        test_var = 'variab'
        test_indent = '    '
        test_line = test_indent + 'if something and other:'
        input_var = {'name': test_var, 'desc': 'Form data', 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': []}
        result = utils.check_for_method(test_line, input_var, data4test.TEST_CONFIG['KNOWN_CHECK_METHODS'])
        self.assertFalse(result)
        self.assertEqual(input_var['status'], utils.VARIABLE_STATUS['unknown'])

    def test_check_safe_fields_one(self):
        test_var = 'variab'
        test_field = 'field'
        test_params = ['{}["{}"])'.format(test_var, test_field)]
        input_var = {'name': test_var, 'desc': 'Form data', 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': [], 'children': []}
        result = utils.check_safe_fields(input_var, test_params)
        self.assertFalse(result)
        input_var['safe_fields'].append({'name': test_field, 'indent': 4})
        result = utils.check_safe_fields(input_var, test_params)
        self.assertTrue(result)

    def test_check_safe_fields_two(self):
        test_var = 'variab'
        test_field = 'field'
        field_two = 'two'
        test_params = ['{}["{}"]'.format(test_var, test_field), '{}["{}"]'.format(test_var, field_two)]
        input_var = {'name': test_var, 'desc': 'Form data', 'status': utils.VARIABLE_STATUS['unknown'],
                     'safe_fields': [{'name': test_field, 'indent': 4}], 'children': []}
        result = utils.check_safe_fields(input_var, test_params)
        self.assertFalse(result)
        input_var['safe_fields'].append({'name': field_two, 'indent': 4})
        result = utils.check_safe_fields(input_var, test_params)
        self.assertTrue(result)

    def test_get_valid_params_single(self):
        test_str = 'first, second["field"], request.json["name"], request.get_data()'
        test_var = 'second'
        expected = ['second["field"]']
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, expected)
        test_var = 'request.json'
        expected = ['request.json["name"]']
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, expected)
        test_var = 'request.get_data'
        expected = ['request.get_data()']
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, expected)

    def test_get_valid_params_format(self):
        test_var = 'name'
        test_str = "'{} gave no name or address'.format(%s)" % test_var
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, [test_str])

    def test_get_valid_params_multi(self):
        test_str = 'second["field"], second["name"], request.args.get("name", ""), request.args.get("id", "")'
        test_var = 'second'
        expected = ['second["field"]', 'second["name"]']
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, expected)
        test_var = 'request.args.get'
        expected = ['request.args.get("name", "")', 'request.args.get("id", "")']
        param_list = utils.get_valid_params(test_str, test_var)
        self.assertListEqual(param_list, expected)

    def test_concat(self):
        """
        Test concatenation of a multiline if statement
        """
        test_lines = data4test.MULTI_IF
        combo, index = utils.concat_multiline_if(test_lines, 1)
        self.assertEqual(len(combo), 108)
        self.assertEqual(index, 3)
        test_lines = data4test.SINGLE_IF
        combo, index = utils.concat_multiline_if(test_lines, 1)
        self.assertEqual(combo, test_lines[0])
        self.assertEqual(index, 1)
        test_lines = data4test.FAIL_IF
        combo, index = utils.concat_multiline_if(test_lines, 1)
        self.assertEqual(combo, test_lines[0])
        self.assertEqual(index, 1)
        multiline = data4test.FAIL_IF.copy()
        multiline.extend(['   plus line', '   plus line', '   plus line'])
        combo, index = utils.concat_multiline_if(multiline, 1)
        self.assertEqual(combo, test_lines[0])
        self.assertEqual(index, 1)
        test_lines = ['Dummy line without expected statement']
        combo, index = utils.concat_multiline_if(test_lines, 1)
        self.assertEqual(combo, test_lines[0])
        self.assertEqual(index, 1)

    def test_line_completion(self):
        for item in data4test.MULTILINE_FUNC:
            test_lines = item['lines']
            test_index = 1
            line, index = utils.line_completion(test_lines, test_index)
            self.assertEqual(len(line), item['length'])
            self.assertEqual(index, test_index + item['offset'])

    @patch('builtins.print')
    def test_error_print(self, print_mock):
        test_msg = 'Test error message'
        expected = [('\nPapaia error:',), (test_msg,), ('\n',)]
        utils.error_print(test_msg)
        self.assertEqual(print_mock.call_count, 3)
        for index, item in enumerate(print_mock.call_args_list):
            self.assertEqual(item[0], expected[index])


if __name__ == '__main__':
    unittest.main()
