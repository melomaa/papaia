import unittest
from mock import patch, MagicMock, sentinel
from datetime import datetime
import re
import papaia.reporter as reporter

TEST_STATS = {'routes': 10, 'parsed': 8, 'filtered': 4, 'inputs': 8,
              'misses': 0, 'safe': 1, 'unknown': 1, 'concerns': 2}

TEST_CONCERN_STR = """
/test (line 63)
-------------------

 * Test concern one
 * Test concern two

"""


class TestReporter(unittest.TestCase):

    def test_render_stats(self):
        open_inputs = TEST_STATS['inputs'] - TEST_STATS['filtered'] - TEST_STATS['misses'] - TEST_STATS['safe']
        output = reporter.render_stats(TEST_STATS)
        for item in TEST_STATS.values():
            self.assertTrue(str(item) in output)

        def validate_value(label, expected):
            actual = re.search(label + ': ([0-9]+)', output).group(1)
            self.assertEqual(actual, str(expected))

        validate_value('Warnings', TEST_STATS['concerns'])
        validate_value('Open', open_inputs)
        validate_value('Unparsed', TEST_STATS['routes'] - TEST_STATS['parsed'])

    def test_render_concerns(self):
        test_data = {'routes': {
            '/login': {'line': 25, 'concerns': []},
            '/test': {'line': 63, 'concerns': ['Test concern one', 'Test concern two']}}}
        output, count = reporter.render_concerns(test_data)
        self.assertEqual(count, 1)
        self.assertEqual(output.count('\n'), 6)

    @patch('papaia.reporter.Environment')
    @patch('papaia.reporter.FileSystemLoader')
    @patch('papaia.reporter.PackageLoader')
    def test_generate_html_report(self, pkg_mock, file_mock, env_mock):
        pkg_mock.return_value = sentinel.packageloader
        file_mock.return_value = sentinel.fileloader
        mock_template = MagicMock()
        mock_template.render.return_value = sentinel.render
        mock_jinja = MagicMock()
        mock_jinja.get_template.return_value = mock_template
        env_mock.return_value = mock_jinja
        test_file = 'test_file.html'
        test_date = '11/22/33'
        local_stats = TEST_STATS.copy()
        result = reporter.generate_html_report(test_file, TEST_CONCERN_STR, local_stats, test_date)
        self.assertEqual(result, sentinel.render)
        self.assertEqual(local_stats['unparsed'], 2)
        self.assertEqual(local_stats['open'], 3)
        kwargs = mock_template.render.call_args.kwargs
        self.assertEqual(kwargs['file'], test_file)
        self.assertTrue(kwargs['concerns'].startswith('<h2>/test (line 63)'))
        self.assertEqual(kwargs['date'], test_date)

    @patch('papaia.reporter.Environment')
    @patch('papaia.reporter.FileSystemLoader')
    @patch('papaia.reporter.PackageLoader')
    def test_generate_html_report_pkg_exp(self, pkg_mock, file_mock, env_mock):
        pkg_mock.side_effect = ModuleNotFoundError
        file_mock.return_value = sentinel.fileloader
        mock_template = MagicMock()
        mock_template.render.return_value = sentinel.render
        mock_jinja = MagicMock()
        mock_jinja.get_template.return_value = mock_template
        env_mock.return_value = mock_jinja
        test_file = 'test_file.html'
        test_date = '11/22/33'
        local_stats = TEST_STATS.copy()
        result = reporter.generate_html_report(test_file, TEST_CONCERN_STR, local_stats, test_date)
        self.assertEqual(result, sentinel.render)
        self.assertEqual(local_stats['unparsed'], 2)
        self.assertEqual(local_stats['open'], 3)
        kwargs = mock_template.render.call_args.kwargs
        self.assertEqual(kwargs['file'], test_file)
        self.assertTrue(kwargs['concerns'].startswith('<h2>/test (line 63)'))
        self.assertEqual(kwargs['date'], test_date)


    @patch('builtins.print')
    @patch('papaia.reporter.generate_html_report')
    @patch('papaia.reporter.render_stats')
    @patch('papaia.reporter.render_concerns')
    def test_output_database(self, rdr_mock, stats_mock, html_mock, print_mock):
        test_concern_str = 'test concern string'
        rdr_mock.return_value = (test_concern_str, 2)
        stats_mock.return_value = ''
        test_output = 'test html report'
        html_mock.return_value = test_output
        test_time = datetime(2020, 9, 9, 17, 9, 59)
        test_server = 'server.py'
        test_database = {test_server: {'stats': {}, 'date': test_time}}
        expected_output = "API report for " + test_server + "\n====================\n"
        expected_output += "\nGenerated on  09/09/2020 at 17:09:59\n\n" + test_concern_str + "\n\n"
        reporter.output_database(test_database)
        print_mock.assert_called_with(expected_output)

    @patch('papaia.reporter.generate_html_report')
    @patch('papaia.reporter.render_stats')
    @patch('papaia.reporter.render_concerns')
    def test_output_database_html_file(self, rdr_mock, stats_mock, html_mock):
        test_concern_str = 'test concern string'
        rdr_mock.return_value = (test_concern_str, 2)
        stats_mock.return_value = ''
        test_output = 'test html report'
        html_mock.return_value = test_output
        test_time = datetime.now()
        test_database = {'server.py': {'stats': {}, 'date': test_time}}
        test_out_file = '/tmp/test_file.html'
        with patch('papaia.reporter.open', create=True) as mock_open:
            reporter.output_database(test_database, 'html', test_out_file)
            mock_open().__enter__().write.assert_called_with(test_output)


if __name__ == '__main__':
    unittest.main()
