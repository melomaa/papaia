"""
Threat cases for API parser testing
"""

UNSAFE_VARIABLE_USES = [
    {'lines': """
    #    Tests 1.1, 1.7, 2.2  
    #    Route with GET and POST methods.
    #    The 'id' argument of GET request is used without validation.
    #    The JSON data of the POST request is used without validation.
    @server.route("/analyze", methods=['GET', 'POST'])
    def analyze():
        if request.method == 'GET':
            uid = request.args.get('id', '')
            result = get_result_by_uid(uid)
        else:
            new_data = request.get_json()
            result = run_analysis(new_data)
        return result
    """, 'route': '/analyze', 'concerns': 2},
    {'lines': """
    #    Tests 1.1, 2.2
    #    Route without methods definition (defaults to GET).
    #    One input argument, which is validated with known filter before use
    #    But debug print uses the variable before validation
@server.route("/person")
def get_person():
    reply = '{}'
    code = 400
    name = request.args.get('name', '')
    print('Validity of {} is checked after this'.format(name))
    if name and whitelist_input(name):
        reply = get_file_for_person(name)
        code = 200
    return reply, code
    """, 'route': '/person', 'concerns': 1},
    {'lines': """
    #    Tests 1.3, 2.2
    #    Request values are passed to a function for obtaining specific attribute.
    #    If extract_named_arg is not marked safe in configuration, this is a threat.
@server.route("/locate", methods=['GET'])
def locate():
    values = request.values
    name = extract_named_arg(values, 'name')
    unsafe_func(name)
    return "OK", 200
    """, 'route': '/locate', 'concerns': 1},
    {'lines': """
    #    Tests 1.4, 2.2
    #    The form property of request class is assigned to variable and
    #    passed to a function without validation.
@server.route("/submit", methods=['POST'])
def submit():
    form_data = request.form
    unsafe_func(form_data)
    return "OK", 200
    """, 'route': '/submit', 'concerns': 1},
    {'lines': """
    #    Tests 1.5, 2.2
    #    Value extracted from the json property of request class.
    #    The extracted value is not validated before use.
@server.route("/store", methods=['POST'])
def store():
    code = 400
    uid = request.json['uid']
    if request.json['update']:
        change_name(uid)
    return "OK", code
    """, 'route': '/store', 'concerns': 1},
    {'lines': """
    #    Tests 1.7, 2.2
    #    Route with PUT method.
    #    The input data of the PUT request is used without validation.
    @server.route("/update", methods=['PUT'])
    def update():
        code = 400
        update_data = request.get_json()
        if perform_update(update_data):
            code = 200
        return "{}", code
    """, 'route': '/update', 'concerns': 1},
    {'lines': """
    @server.route("/song")
    def get_song():
        reply = json.dumps({'status': -2})
        code = 400
        title = request.args.get('title', '')
        album = request.args.get('album', '')
        if title and whitelist_input(title):
            reply = json.dumps(song_data(title), album)
            code = 200
        return reply, code
    """, 'route': '/song', 'concerns': 1},
]

DIRECT_INPUT = [
    {'lines': """
    # Tests 1.1, 2.1
    # Route without methods definition (defaults to GET).
    # The 'brand' argument is validated before use.
    # The 'model' argument is used without validation.
    @server.route("/bike_info")
    def bike_info():
        bike_data = '{}'
        code = 400
        brand = request.args.get('brand', '')
        if brand.isalnum():
            bike_data, code = get_bike_info(brand, request.args.get('model', ''))
        return bike_data, code
    """, 'route': '/bike_info', 'concerns': 1},
    {'lines': """
    #    Tests 1.2, 2.1
    #    Request query string is passed to a function for obtaining specific attribute.
    #    If extract_named_arg is not marked safe in configuration, this is a threat.
    @server.route("/local", methods=['GET'])
    def localise():
        name = extract_named_arg(request.query_string, 'name')
        unsafe_func(name)
        return "OK", 200
    """, 'route': '/local', 'concerns': 1},
    {'lines': """
    #    Tests 1.6, 2.1
    #    Route with POST method.
    #    The input data of the POST request is used without validation.
    #    This route shall be shown in the list of concerns.
    @server.route("/create", methods=['POST'])
    def create():
        code = 400
        if create_user(request.get_data()):
            code = 200
        return "{}", code
    """, 'route': '/create', 'concerns': 1},
    {'lines': """
    #    Tests 1.4, 2.1
    #    A value is extracted from the form property of request class and
    #    directly passed to a function without validation.
    @server.route("/apply", methods=['POST'])
    def apply():
        unsafe_func(request.form['name'])
        return "OK", 200
    """, 'route': '/apply', 'concerns': 1},
    {'lines': """
    #    Tests 1.5, 2.1
    #    Input is validated before passing to first function, but later
    #    directly passed to a function without validation.
    @server.route("/reply", methods=['POST'])
    def reply():
        if request.json['name'].isalnum():
            unsafe_func(request.json['name'])
        another_func(request.json['name'])
        return "OK", 200
    """, 'route': '/reply', 'concerns': 1},
    {'lines': """
    @server.route("/band")
    def get_band():
        reply = json.dumps({'status': -2})
        reply, code = json.dumps(safe_func(request.json), band_data(request.json))
        return reply, code
    """, 'route': '/band', 'concerns': 1},
]

FORMAT_STRING = [
    {'lines': """
    #    Tests 1.1, 2.3
    #    Route without methods definition (defaults to GET).
    #    The value extracted from request arguments is used (passed to print) before validation.
    @server.route("/info")
    def info():
        person_info = "{}"
        req_args = request.args
        name = req_args.get('name', '')
        print('Value of name is %s' % name)
        if name.isalnum():
            person_info = unsafe_func(name)
        return person_info, 200
    """, 'route': '/info', 'concerns': 1}
]
