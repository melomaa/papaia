# coding=utf-8
"""
Unit test runner
"""
import os
import argparse
import unittest
import coverage

TEST_MODULES = [
    'test_apiparser.TestParser',
    'test_utils.TestStatic',
    'test_reporter.TestReporter',
    'test_collector.TestCollector',
    'test_main.TestMain'
]

COV = coverage.coverage(
    branch=True,
    include='./papaia/*.py',
    omit=[
        './tests/*',
        './samples/*',
        './dist/*',
        './build/*',
        '*setup.*',
        '*__init__.py',
        '*.html'
    ]
)

modules = TEST_MODULES


def run_tests(split):
    """
    Run unit tests for listed modules

    :param split: Run test modules separately
    """
    COV.start()

    if split:
        for module in modules:
            suite = unittest.TestSuite()
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(module))
            print('\n' + module)
            unittest.TextTestRunner().run(suite)
    else:
        suite = unittest.TestSuite()
        for module in modules:
            try:
                # If the module defines a suite() function, call it to get the suite.
                mod = __import__(module, globals(), locals(), ['suite'])
                suitefn = getattr(mod, 'suite')
                suite.addTest(suitefn())
            except (ImportError, AttributeError):
                suite.addTest(unittest.defaultTestLoader.loadTestsFromName(module))
        unittest.TextTestRunner().run(suite)

    COV.stop()
    COV.save()


def main(out_format, quiet=False, split=False):
    """
    Main function for running and reporting

    :param out_format: Report format
    :param quiet: Silence summary
    :param split: Run test modules separately
    """
    run_tests(split)
    if not quiet:
        print('Coverage Summary:')
        COV.report()

    basedir = os.path.abspath(os.path.dirname(__file__))
    if out_format == 'html':
        coverage_dir = os.path.join(basedir, 'coverage')
        COV.html_report(directory=coverage_dir)
    elif out_format == 'xml':
        xml_path = os.path.join(basedir, 'papaia-coverage.xml')
        COV.xml_report(outfile=xml_path)


def init_argparse():
    """
    Initialize command line argument parsing

    :return: Argument parser instance
    """
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [FILE]...",
        description="Run unit tests."
    )
    parser.add_argument(
        "-v", "--version", action="version", version="Version 1.0"
    )
    parser.add_argument(
        "-f", "--format", nargs='?', default='xml',
        choices=['xml', 'html', 'none'], help="Output format"
    )
    parser.add_argument(
        "-q", "--quiet", action='store_true', help="Do not print summary"
    )
    parser.add_argument(
        "-s", "--split", action='store_true', help="Run each test module separately"
    )
    parser.add_argument('modules', nargs='*',
                        help="Set of test modules to run. If none given all known modules are run.")
    return parser


if __name__ == '__main__':
    args = init_argparse().parse_args()
    if args.modules:
        modules = []
        for test_module in args.modules:
            modules.append(test_module)
    main(args.format, args.quiet, args.split)
    COV.erase()
