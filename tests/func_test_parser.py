import unittest
import data4test
import negative_cases
import positive_cases
from api_parser import ApiParser

EMPTY_STATS = {'routes': 0, 'filtered': 0, 'inputs': 0,
               'misses': 0, 'safe': 0, 'unknown': 0}


class ParserNegative(unittest.TestCase):

    def setUp(self):
        self.parser = ApiParser(data4test.TEST_CONFIG)
        self.parser.server_name = 'server'
        self.parser.stats = EMPTY_STATS.copy()

    def test_unsafe_variable(self):
        for item in negative_cases.UNSAFE_VARIABLE_USES:
            self.parser.analyse_lines(item['lines'].splitlines())
            self.assertEqual(len(self.parser.routes[item['route']]['concerns']), item['concerns'])

    def test_direct_input(self):
        for item in negative_cases.DIRECT_INPUT:
            self.parser.analyse_lines(item['lines'].splitlines())
            self.assertEqual(len(self.parser.routes[item['route']]['concerns']), item['concerns'])

    def test_format_string(self):
        for item in negative_cases.FORMAT_STRING:
            self.parser.analyse_lines(item['lines'].splitlines())


class ParserPositive(unittest.TestCase):

    def setUp(self):
        self.parser = ApiParser(data4test.TEST_CONFIG)
        self.parser.server_name = 'server'

    def test_filtered(self):
        self.parser.stats = EMPTY_STATS.copy()
        self.parser.routes = {}
        for item in positive_cases.FILTERED:
            expected = self.parser.stats['inputs'] + item['inputs']
            self.parser.analyse_lines(item['lines'].splitlines())
            self.assertListEqual(self.parser.routes[item['route']]['concerns'], [])
            self.assertEqual(self.parser.stats['inputs'], expected)
            self.assertEqual(self.parser.stats['filtered'], expected)

    def test_safe(self):
        self.parser.stats = EMPTY_STATS.copy()
        self.parser.routes = {}
        for item in positive_cases.SAFE:
            expected = self.parser.stats['inputs'] + item['inputs']
            self.parser.analyse_lines(item['lines'].splitlines())
            self.assertListEqual(self.parser.routes[item['route']]['concerns'], [])
            self.assertEqual(self.parser.stats['inputs'], expected)
            self.assertEqual(self.parser.stats['safe'], expected)


if __name__ == '__main__':
    unittest.main()
