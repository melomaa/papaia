"""
Positive cases for API parser testing
"""

FILTERED = [
    {'lines': """
    #    Tests 1.1, 3.2
    #    Route without methods definition (defaults to GET).
    #    One input argument, which is validated with known filter before use
@server.route("/person")
def get_person():
    reply = '{}'
    code = 400
    name = request.args.get('name', '')
    if name and whitelist_input(name):
        reply = get_file_for_person(name)
        code = 200
    return reply, code
    """, 'route': '/person', 'inputs': 1},
    {'lines': """
    #    Tests 1.1, 3.2
    #    Route without methods definition (defaults to GET).
    #    One input argument, which is validated with known filter before use
@server.route("/title")
def get_title():
    reply = '{}'
    code = 400
    if request.args.get('title', '').isalnum():
        reply = get_book(request.args.get('title', ''))
        code = 200
    return reply, code
    """, 'route': '/title', 'inputs': 1},
    {'lines': """
    #    Tests 1.5, 3.2
    #    One input argument, which is validated with known filter before use
@server.route("/find", methods=['POST'])
def find_person():
    reply = '{}'
    code = 400
    if whitelist_input(request.json.get('id')):
        reply = get_file_for_person(request.json['id'])
        code = 200
    return reply, code
    """, 'route': '/find', 'inputs': 1},
    {'lines': """
    #    Tests 1.5, 3.2
    #    One input argument, which is validated with known filter before use
@server.route("/file", methods=['POST'])
def file_person():
    reply = '{}'
    code = 400
    if whitelist_input(request.json['id']):
        reply = get_file_for_person(some_var, request.json.get('id', ''))
        code = 200
    return reply, code
    """, 'route': '/file', 'inputs': 1},
    {'lines': """
    @server.route("/song")
    def get_song():
        reply = json.dumps({'status': -2})
        code = 400
        title = request.args.get('title', '')
        if title and whitelist_input(title):
            reply = json.dumps(song_data(title))
            code = 200
        return reply, code
    """, 'route': '/song', 'inputs': 1},
]

SAFE = [
    {'lines': """
    #    Tests 1.6, 2.1
    #    Input directly passed to a safe function without validation.
    @server.route("/apply", methods=['POST'])
    def apply():
        safe_func(request.get_data())
        return "OK", 200
    """, 'route': '/apply', 'inputs': 1},
    {'lines': """
    #    Tests 1.6, 2.1
    #    Input directly passed to a safe function without validation.
    #    Input variable name in a string is not a threat
    @server.route("/reply", methods=['POST'])
    def reply():
        safe_func(request.get_data())
        print("request.json")
        print('Just request.args text')  
        return "OK", 200
    """, 'route': '/reply', 'inputs': 1},
    {'lines': """
    #    Tests 1.6, 2.1
    #    Multiple inputs directly passed to a safe function without validation.
    @server.route("/reply", methods=['POST'])
    def reply():
        safe_func(request.get_data(), json_data=request.json)
        return "OK", 200
    """, 'route': '/reply', 'inputs': 2},
]
