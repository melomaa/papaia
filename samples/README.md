Example server
==============

This directory contains an example server for testing purposes. There is also a Papaia configuration file that can be used for testing or as an example for your own configuration.

The robot directory contains a Robot Framework example for automated API testing.

### Requirements

Papaia depends on Jinja2, Markdown and pyYAML packages. The requirements can be installed using the requirements.txt provided in the sources.

    <SOURCE_ROOT>$ pip install -r requirements.txt

### Usage


Usage with downloaded source files:

    <SOURCE_ROOT>$ PYTHONPATH=. python3 papaia/main.py -h
    
Produce HTML report on the sample server using the sample config:
    
    <SOURCE_ROOT>$ PYTHONPATH=. python3 papaia/main.py -f html -c samples/papaia_config.yaml -o report.html samples/sample_server.py
    
Usage when papaia is installed as a package:

    papaia -h

