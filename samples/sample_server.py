"""
Example server file for API parsing
"""
import re
from flask import Flask, request

server = Flask(__name__)

# config
server.config.update(
    SECRET_KEY='qazwsxedcrfvtgbyhn'
)


def whitelist_input(data):
    """
    Check that input string is safe.
    Allowed characters  are alphanumeric, underscore, whitespace and hyphen

    :param data: String to be checked
    :return: True if string is safe
    """
    result = False
    if isinstance(data, str):
        filter_regex = re.compile('[^\\w\\s-]')
        check = filter_regex.search(data)
        if check is None:
            result = True
    return result


@server.route("/")
def load_main():
    """
    Route with no input
    """
    return render_template('main.html')


@server.route("/analyze", methods=['GET', 'POST'])
def analyze():
    """
    Route with GET and POST methods.
    The 'id' argument of GET request is used without validation.
    The JSON data of the POST request is used without validation.
    This route shall be shown in the list of concerns.
    """
    if request.method == 'GET':
        uid = request.args.get('id', '')
        result = get_result_by_uid(uid)
    else:
        new_data = request.get_json()
        result = run_analysis(new_data)
    return result


@server.route("/person")
def get_person():
    """
    Route without methods definition (defaults to GET).
    One input argument, which is validated with known filter before use
    """
    reply = '{}'
    code = 400
    name = request.args.get('name', '')
    print('Validity of name is checked after this')
    if name and whitelist_input(name):
        reply = get_file_for_person(name)
        code = 200
    return reply, code


@server.route("/update", methods=['PUT'])
def update():
    """
    Route with PUT method.
    The input data of the PUT request is used without validation.
    This route shall be shown in the list of concerns,
    unless perform_update is listed in KNOWN_SAFE section of the configuration file.
    """
    code = 400
    update_data = request.get_data()
    if perform_update(update_data):
        code = 200
    return "{}", code


@server.route("/bike_info")
def bike_info():
    """
    Route without methods definition (defaults to GET).
    The 'brand' argument is validated before use.
    The 'model' argument is used without validation.
    This route shall be shown in the list of concerns.
    """
    bike_data = '{}'
    code = 400
    brand = request.args.get('brand', '')
    if brand.isalnum():
        bike_data, code = get_bike_info(brand, request.args.get('model', ''))
    return bike_data, code


@server.route("/create", methods=['POST'])
def create():
    """
    Route with POST method.
    The input data of the POST request is used without validation.
    This route shall be shown in the list of concerns.
    """
    code = 400
    if create_user(request.get_data()):
        code = 200
    return "{}", code


@server.route("/info")
def info():
    """
    Route without methods definition (defaults to GET).
    The value extracted from request arguments is used (passed to print) before validation.
    This route shall be shown in the list of concerns.
    """
    person_info = "{}"
    req_args = request.args
    name = req_args.get('name', '')
    print('Value of name is %s' % name)
    if name.isalnum():
        person_info = unsafe_func(name)
    return person_info, 200


@server.route("/change", methods=['PUT'])
def change():
    """
    Route with PUT method.
    The required fields in input data of the PUT request are validated before use.
    """
    code = 400
    data = request.get_json()
    if 'id' in data and data['id'].isnumeric() and \
       'name' in data and data['name'].isalnum():
        change_name(data['id'], data['name'])
        code = 200
    return "{}", code


@server.route("/submit", methods=['POST'])
def submit():
    """
    The form property of request class is assigned to variable and
    passed to a function without validation.
    This route shall be shown in the list of concerns.
    """
    form_data = request.form
    unsafe_func(form_data)
    return "OK", 200


@server.route("/apply", methods=['POST'])
def apply():
    """
    A value is extracted from the form property of request class and
    directly passed to a function without validation.
    This route shall be shown in the list of concerns.
    """
    unsafe_func(request.form['name'])
    return "OK", 200


@server.route("/store", methods=['POST'])
def store():
    """
    Value extracted from the json property of request class.
    The extracted value is validated before first use,
    but used later outside validated content
    """
    code = 400
    uid = request.json['uid']
    if uid.isalnum():
        code = unsafe_func(uid)
    if request.json['update']:
        change_name(uid)
    return "OK", code


@server.route("/locate", methods=['GET'])
def locate():
    """
    Request values are passed to a function for obtaining specific attribute.
    If extract_named_arg is not marked safe in configuration,
    this route shall be shown in the list of concerns.
    """
    values = request.values
    name = extract_named_arg(values, 'name')
    unsafe_func(name)
    return "OK", 200


if __name__ == "__main__":
    server.run()
