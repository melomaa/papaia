Robot Framework Example
=======================

This directory contains an example how to use the API information collected by Papaia with Robot Framework in order to run some negative tests on API routes.

Contents:

  * __api_data.json__ : API routes collected from the example server and populated with some test data.
  * __API_variables.py__ : Robot Framework variable file containing declarations for variables used in the example test suite, such as name of the API data file and strings to be used as test data.
  * __api.robot__ : Robot Framework test suite with test cases and keywords for testing requests with given data


The API routes and related information is collected running Papaia with the -t switch

    python3 papaia/main.py -t -o samples/robot/api_data.json samples/sample_server.py

The test suite contains separate test cases for GET, POST and PUT methods. Each test case loops through the routes in the data file and sends requests to the server using the populated data. Routes which do not have data are skipped.

Running the sample server requires flask package and robot tests require robotframework and robotframework-requests packages. These can be installed with pip:

    pip install flask robotframework-requests
