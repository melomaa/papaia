*** Settings ***
Library            String
Library            Collections
Library            RequestsLibrary
Variables          API_variables.py
Suite Setup        Open Session

*** Variables ***

*** Test Cases ***
Check Get Requests
    FOR    ${route}    IN    @{API_DATA.keys()}
        Run Keyword If   "GET" in $API_DATA["${route}"]  Check Get Request    ${route}    ${API_DATA["${route}"]["GET"]}
    END

Check Post Requests
    FOR    ${route}    IN    @{API_DATA.keys()}
        Run Keyword If   "POST" in $API_DATA["${route}"]  Check Post Request    ${route}    ${API_DATA["${route}"]["POST"]}
    END

Check Put Requests
    FOR    ${route}    IN    @{API_DATA.keys()}
        Run Keyword If   "PUT" in $API_DATA["${route}"]  Check Put Request    ${route}    ${API_DATA["${route}"]["PUT"]}
    END


*** Keywords ***
Open Session
    Create Session    api    ${SERVER_URL}
    ${API_DATA}    Evaluate    json.load(open("${API_DATA_FILE}", "r"))    json
    Set Suite Variable    ${API_DATA}

Check Get Request
    [Arguments]    ${route}    ${get_data}
    ${num_tests}=    Get Length    ${get_data['status']}
    ${args}=    Get Dictionary Keys    ${get_data}
    Remove Values From List    ${args}    status
    ${num_args}=    Get Length    ${args}
    FOR    ${idx}    IN RANGE    ${num_tests}
        ${arg_str}=    Run Keyword If    ${num_args} > 0    Parse Args    ${get_data}    ${args}    ${idx}    ${num_args}
        ${resp}=          Get Request    api    ${route}    params=${arg_str}
        Status Should Be    ${get_data['status'][${idx}]}            ${resp}
    END

Check Post Request
    [Arguments]    ${route}    ${req_data}
    ${num_tests}=    Get Length    ${req_data['status']}
    FOR    ${idx}    IN RANGE    ${num_tests}
        ${json_val}=    Get Arg Value    ${req_data["data"][${idx}]}
        ${resp}=          Post Request    api    ${route}    json=${json_val}
        Status Should Be    ${req_data['status'][${idx}]}            ${resp}
    END

Check Put Request
    [Arguments]    ${route}    ${req_data}
    ${num_tests}=    Get Length    ${req_data['status']}
    FOR    ${idx}    IN RANGE    ${num_tests}
        ${json_val}=    Get Arg Value    ${req_data["data"][${idx}]}
        ${resp}=          Put Request    api    ${route}    json=${json_val}
        Status Should Be    ${req_data['status'][${idx}]}            ${resp}
    END

Parse Args
    [Arguments]    ${get_data}    ${args}    ${idx}    ${num_args}
    ${aval}=    Get Arg Value    ${get_data["${args}[0]"][${idx}]}
    ${arg_str}=    Set Variable    ${args}[0]=${aval}
    FOR    ${argidx}    IN RANGE    1    ${num_args}
        ${aval}=    Get Arg Value    ${get_data["${args}[${argidx}]"][${idx}]}
        ${arg_str}=    Set Variable    ${arg_str}&${args}[${argidx}]=${aval}
    END
    [Return]    ${arg_str}

Get Arg Value
    [Arguments]    ${raw}
    ${is_var}=    Run Keyword And Return Status    Should Start With    ${raw}    \$
    ${arg_value}=    Run Keyword If        ${is_var}    Expand Variable    ${raw}    ELSE    Set Variable    ${raw}
    [Return]    ${arg_value}

Expand Variable
    [Arguments]    ${raw}
    ${var_name}=    Get Substring    ${raw}    2    -1
    [Return]    ${${var_name}}
