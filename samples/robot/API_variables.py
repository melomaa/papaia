API_DATA_FILE="api_data.json"
SERVER_URL="http://localhost:5000"

# Input for negative tests
UNSAFE_STR="name; ls -l"
NON_ALNUM="name-name"
NON_NUMERIC="123abc"